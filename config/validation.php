<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'checkout' => [
        'rules' => [
            'recipient' => 'required|in:guest,me,other',
            'name' => 'required|string',
            'national_id' => 'required|min:10|max:10',
            'mobile' => 'required|min:11|max:11',
            'province' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'postal_code' => 'required|min:10|max:10',
            'description' => '',
        ],
        'messages' => [

            'recipient.required' => 'فیلد فرد گیرنده را لطفا پر کنید.',
            'recipient.in' => 'مقدار فیلد فرد گیرنده مجاز نیست.',

            'name.required' => 'فیلد نام و نام خانوادگی را لطفا پر کنید.',
            'name.string' => 'فیلد نام و نام خانوادگی را فارسی پر کنید.',

            'national_id.required' => 'فیلد شماره ملی را لطفا پر کنید.',
            'national_id.min' => 'ساختار شماره ملی حداقل 10 رقمی ست.',
            'national_id.max' => 'ساختار شماره ملی حداکثر 10 رقمی ست.',

            'mobile.required' => 'فیلد شماره موبایل را لطفا پر کنید.',
            'mobile.min' => 'ساختار شماره موبایل حداقل 11 رقمی ست.',
            'mobile.max' => 'ساختار شماره موبایل حداکثر 11 رقمی ست.',

            'province.required' => 'فیلد استان را لطفا پر کنید.',
            'province.string' => 'فیلد استان را فارسی پر کنید.',

            'city.required' => 'فیلد شهر را لطفا پر کنید.',
            'city.string' => 'فیلد شهر را فارسی پر کنید.',

            'address.required' => 'فیلد آدرس را لطفا پر کنید.',
            'address.string' => 'فیلد آدرس را فارسی پر کنید.',

            'postal_code.required' => 'فیلد کد پستی را لطفا پر کنید.',
            'postal_code.min' => 'ساختار کد پستی حداقل 10 رقمی ست.',
            'postal_code.max' => 'ساختار کد پستی حداکثر 10 رقمی ست.',

        ]
    ],


    'change_password' => [
        'rules' => [
            'current_password' => 'required|string|min:8',
            'password' => 'required|string|confirmed|min:8',
        ],
        'messages' => [
            'current_password.required' => 'فیلد رمز عبور فعلی را لطفا پر کنید.',
            'current_password.min' => 'رمز عبور حداقل 8 کارکتر است.',

            'password.required' => 'فیلد رمز عبور جدید را لطفا پر کنید.',
            'password.min' => 'رمز عبور حداقل 8 کارکتر است.',
            'password.confirmed' => 'تکرار رمز عبور مطابقتی با رمز عبور ندارد.',
        ]
    ],

    'user' => [
        'rules' => [
            'name' => ['required'],
            'national_id' => 'required|min:10|max:10',
            'province' => 'required|string',
            'city' => 'required|string',
            'email' => 'required|string',
            'address' => 'required|string',
            'postal_code' => 'required|min:10|max:10',
        ],
        'messages' => [

            'name.required' => 'فیلد نام و نام خانوادگی را لطفا پر کنید.',
            'name.string' => 'فیلد نام و نام خانوادگی را فارسی پر کنید.',

            'national_id.required' => 'فیلد شماره ملی را لطفا پر کنید.',
            'national_id.min' => 'ساختار شماره ملی حداقل 10 رقمی ست.',
            'national_id.max' => 'ساختار شماره ملی حداکثر 10 رقمی ست.',

            'province.required' => 'فیلد استان را لطفا پر کنید.',
            'province.string' => 'فیلد استان را فارسی پر کنید.',

            'city.required' => 'فیلد شهر را لطفا پر کنید.',
            'city.string' => 'فیلد شهر را فارسی پر کنید.',

            'email.required' => 'فیلد ایمیل را لطفا پر کنید.',
            'email.string' => 'فیلد ایمیل را رشته وارد کنید.',

            'address.required' => 'فیلد آدرس را لطفا پر کنید.',
            'address.string' => 'فیلد آدرس را فارسی پر کنید.',

            'postal_code.required' => 'فیلد کد پستی را لطفا پر کنید.',
            'postal_code.min' => 'ساختار کد پستی حداقل 10 رقمی ست.',
            'postal_code.max' => 'ساختار کد پستی حداکثر 10 رقمی ست.',
        ]
    ],

    'verification' => [
        'messages' => [
            'mobile.required' => 'فیلد شماره موبایل را لطفا پر کنید.',
            'mobile.min' => 'رمز عبور حداقل 11 رقمی است.',
            'mobile.max' => 'رمز عبور حداکثر 11 رقمی است.',
            'mobile.unique' => 'این شماره موبایل قبلا توسط کاربر دیگری ثبت شده است.',
            'mobile.exists' => 'این شماره موبایل ثبت نشده است.',
        ]
    ],


];
