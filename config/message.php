<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'bag' => [
        'added' => 'این محصول به لیست خرید اضافه شد.',
        'duplicated' => 'این محصول را قبلا به لیست خرید اضافه کرده ایید.',
        'removed' => 'آیتم مورد نظر از لیست خرید حذف شد.',
    ],

    'product' => [
        'duplicated' => 'این محصول را قبلا نشان کرده ایید.',
        'wished' => 'این محصول به لیست نشان شده های شما اضافه شد.',
        'unwished' => 'این محصول از لیست نشان شده های شما حذف شد.',
    ],

    'password' => [
        'equality' => 'رمز عبور فعلی به درستی وارد نشده است.',
    ],

    'course' => [
        'duplicated' => 'این آموزش را قبلا نشان کرده ایید.',
        'wished' => 'این آموزش به لیست نشان شده های شما اضافه شد.',
        'unwished' => 'این آموزش از لیست نشان شده های شما حذف شد.',
    ],

    'exception' => [
        'failed' => 'عملیات امکان پذیر نیست',
    ],

];
