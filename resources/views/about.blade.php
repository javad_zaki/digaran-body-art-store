@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>
        <section class="software_web our_story margin-t-12" id="About">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 margin-t-8 margin-b-7">
                        <div class="item__section mb-4 mb-lg-0">
                            <div class="media">
                                {{--<div class="icon_sec">
                                    <img src="{{asset('img/layout/blacklogo.png')}}">
                                </div>--}}
                                <div class="media-body">
                                    <div class="title_sections mb-0">
                                        <div class="logotype">
                                            <img src="{{asset('/img/layout/footer-logo.png')}}">
                                        </div>
                                        {{--<h2 class="margin-t-1">درباره</h2>--}}
                                        <p>{!! nl2br(e($body['about']['description'])) !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-lg-5 mt-4 mt-lg-0 ml-auto">
                        <div class="image_grid">
                            <img class="img-fluid img_one" src="{{asset('img/about/1.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="w-100"></div>
                    <div class="col-lg-5 mt-4 mt-lg-0 mx-auto">
                        <div class="image_grid">
                            <div class="before_line">
                                <img class="img-fluid img_two" src="{{asset('img/about/2.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 mt-4 mt-lg-0 ml-auto">
                        <div class="image_grid">
                            <img class="img-fluid img_three" src="{{asset('img/about/3.jpg')}}" alt="">
                        </div>
                    </div>--}}
                </div>

                {{--<div class="row margin-t-8">
                    <div class="col-lg-6">
                        <p class="font-s-16 c-gray">
                            در سالهای اخیر، برند دیگران فعالیت خود را گسترش داده است و با چشم اندازی گسترده پا در عرصه تولید ابزارهای موردنیاز تاتو برای هنرمندان و همچنین طراحی و تولید جواهرات زیبا برای مشتری های خود نهاده است.
                        </p>
                    </div>
                    <div class="col-lg-5 ml-auto">
                        <p class="font-s-16 c-gray">
                            فعالیت اصلی دیگران تولید با کیفیت و استاندارد ابزارها و وسایل موردنیاز تاتو و همچنین جواهرات بی نظیر برای بهبود این هنر دیرینه می باشد.
                        </p>
                    </div>
                </div>--}}

            </div>
        </section>

        {{--<section class="team_static_style margin-t-12" id="Team">
            <div class="container">
                <div class="row justify-content-lg-center">

                    <div class="col-md-6 col-lg-3 item">
                        <div class="item_group aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
                            <div class="img_group">
                                <img src="{{asset('img/about/esi.jpg')}}" alt="">
                            </div>
                            <div class="personal_info">
                                <h3>اسماعیل نصیر زاده</h3>
                                <p>بنیان گذار و مدیر بخش تاتو</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 item mx-auto my-auto">
                        <div class="title_sections_inner aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                            <h2>هدف ما،<br> بهبود هنر تاتو و پیرسینگ است</h2>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 item">
                        <div class="item_group aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                            <div class="img_group">
                                <img src="{{asset('img/about/miki.jpg')}}" alt="">
                            </div>
                            <div class="personal_info">
                                <h3>میکائیل نصیرزاده</h3>
                                <p>بنیان گذار و مدیر بخش پیرسینگ</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>--}}

    </main>
    <!-- Stat main -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection
