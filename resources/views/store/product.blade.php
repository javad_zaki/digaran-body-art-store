@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <!-- Start -->
        <section class="product_review padding-t-5 margin-b-3" id="Features">
            <div class="container">
                <div class="body_features" data-sticky-container>
                    <div class="row">
                        <div class="col-lg-4 mb-5 mb-md-0">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="row">
                                    {{--<div class="col-12 mb-1">
                                        <div class="moc_app popular">--}}
                                            {{--@if(auth()->check())
                                                @if($body['wish'] == 0)
                                                   --}}{{-- <a href="{{route("{$body['prefix']}.store.product.wish",['id' => $body['specifications']['id']])}}" class="btn btn_md_primary btn-block scale border-0 sweep_letter sweep_top bg-success c-white rounded-10">
                                                        <div class="inside_item">
                                                            <span data-hover="این محصول را نشان کن">نشان کردن</span>
                                                        </div>
                                                    </a>--}}{{--
                                                    <a href="{{route("{$body['prefix']}.store.product.wish",['id' => $body['specifications']['id']])}}"><img class="icon_popular" src="{{asset('img/layout/bookmark.svg')}}"></a>
                                                @else
                                                    --}}{{--<a href="{{route("{$body['prefix']}.store.product.wishNot",['id' => $body['specifications']['id']])}}" class="btn btn_md_primary btn-block scale border-0 sweep_letter sweep_top bg-warning c-white rounded-10">
                                                        <div class="inside_item">
                                                            <span data-hover="این محصول را حذف کن">حذف نشان</span>
                                                        </div>
                                                    </a>--}}{{--
                                                    <a href="{{route("{$body['prefix']}.store.product.wishNot",['id' => $body['specifications']['id']])}}"><img class="icon_popular" src="{{asset('img/layout/filled-bookmark.svg')}}"></a>
                                                @endif
                                            @endif--}}
                                            {{--<div class="tab-content" id="v-pills-tabContent">
                                                <div class="tab-pane fade show active" id="v-pills-p" role="tabpanel"
                                                     aria-labelledby="v-pills-p-tab">
                                                    <img class="slide-in-bottom" src="{{$body['image']['primary_file_name']}}" alt="">
                                                </div>
                                                @foreach($body['colors'] as $key => $color)
                                                    <div class="tab-pane fade show" id="v-pills-{{$key}}" role="tabpanel"
                                                         aria-labelledby="v-pills-{{$key}}-tab">
                                                        <img class="slide-in-bottom" src="{{$color['primary_file_name']}}" alt="">
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>--}}
                                    {{--<div class="col-12">
                                        @if(auth()->check())
                                            @if($body['wish'] == 0)
                                                <a href="{{route("{$body['prefix']}.store.product.wish",['id' => $body['specifications']['id']])}}" class="btn btn_md_primary btn-block scale border-0 sweep_letter sweep_top bg-success c-white rounded-10">
                                                    <div class="inside_item">
                                                        <span data-hover="این محصول را نشان کن">نشان کردن</span>
                                                    </div>
                                                </a>
                                            @else
                                                <a href="{{route("{$body['prefix']}.store.product.wishNot",['id' => $body['specifications']['id']])}}" class="btn btn_md_primary btn-block scale border-0 sweep_letter sweep_top bg-warning c-white rounded-10">
                                                    <div class="inside_item">
                                                        <span data-hover="این محصول را حذف کن">حذف نشان</span>
                                                    </div>
                                                </a>
                                            @endif
                                        @endif
                                    </div>--}}
                                    <div class="col-12 mt-1">
                                        {{--<div class="nav nav-pills justify-content-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active" id="v-pills-p-tab" data-toggle="pill" href="#v-pills-p"
                                               role="tab" aria-controls="v-pills-p" aria-selected="true">
                                                <div class="play_vv">
                                                    <div class="scale rounded-circle b color_select no-before no-after" style="background-color: transparent; border: 2px solid #333333"></div>
                                                </div>
                                            </a>
                                            @foreach($body['colors'] as $key => $color)
                                                <a class="nav-link" id="v-pills-{{$key}}-tab" data-toggle="pill" href="#v-pills-{{$key}}"
                                                   role="tab" aria-controls="v-pills-{{$key}}" aria-selected="true">
                                                    <div class="play_vv">
                                                        <div class="scale rounded-circle b color_select no-before no-after"></div>
                                                    </div>
                                                </a>
                                            @endforeach
                                           --}}{{-- @foreach($body['colors'] as $key => $color)
                                                <a class="nav-link" id="v-pills-{{$key}}-tab" data-toggle="pill" href="#v-pills-{{$key}}"
                                                   role="tab" aria-controls="v-pills-{{$key}}" aria-selected="true">
                                                    <div class="play_vv">
                                                        <div class="scale rounded-circle b color_select no-before no-after" style="background-color: {{json_decode($color['property'], true)['hex']}};"></div>
                                                    </div>
                                                </a>
                                            @endforeach--}}{{--
                                        </div>--}}
                                        <section class="product_reviewer padding-t-3 padding-b-5">
                                            <div class="swip__stories ">
                                                <!-- Swiper -->
                                                <div class="swiper-container product_reviewer_slides">
                                                    <div class="swiper-wrapper">
                                                        <div class="swiper-slide">
                                                            <div class="item">
                                                                <div class="img__nature">
                                                                    <img src="{{$body['image']['primary_file_name']}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @foreach($body['colors'] as $color)
                                                            <div class="swiper-slide">
                                                                <div class="item">
                                                                    <div class="img__nature">
                                                                        <img src="{{$color['primary_file_name']}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                    <!-- Add Arrows -->
                                                    <div class="swiper-button-next">
                                                        <i class="tio pe-7s-angle-right"></i>
                                                    </div>
                                                    <div class="swiper-button-prev">
                                                        <i class="tio pe-7s-angle-left"></i>
                                                    </div>

                                                </div>
                                            </div>
                                        </section>

                                        <div class="container-fluid margin-my-1">
                                            <div class="dividar_line"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        @if(session()->has('message'))
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">  {{session()->get('message')}}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <i class="pe-7s-close tio"></i>
                                                </button>
                                            </div>
                                        @endif
                                        @if($body['specifications']['qty'] > 0)
                                            <a href="{{route('shopping.bag.add',['sku' => $body['specifications']['sku']])}}" class="btn btn_md_primary btn-block scale border-0 sweep_letter sweep_top bg-blue c-white rounded-10">
                                                <div class="inside_item">
                                                    <span data-hover="افزودن به لیست خرید">خرید</span>
                                                </div>
                                            </a>
                                        @else
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">  در حال حاضر این محصول موجود نمی باشد.
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <i class="pe-7s-close tio"></i>
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-12">
                                    <div class="banner_title">
                                        <div class="before_title">
                                            <a href="{{url(session()->get('prefix').'/store?category='.$body['specifications']['category_id'])}}" class="btn btn_sm_primary c-blue rounded-12">
                                                <i class="tio pe-7s-back mr-1 font-s-20"></i>
                                                <span>{{$body['specifications']['category']}}</span>
                                            </a>
                                        </div>
                                        <h1>
                                            {{$body['specifications']['name']}}
                                        </h1>
                                        <p>
                                            {{$body['specifications']['short_description']}}
                                        </p>
                                    </div>
                                    <div class="container-fluid margin-my-1">
                                        <div class="dividar_line"></div>
                                    </div>
                                </div>
                                <div class="col-12 margin-t-5">
                                    <div class="item_price item-select">
                                        <div class="part_one">
                                            <span class="check_select"></span>
                                            <h3> قیمت
                                                @if($body['specifications']['price']['discount'] > 0)
                                                    <div class="disable">
                                                        <span class="numeral line-through"> {{number_format($body['specifications']['price']['base_price'])}} </span>تومان
                                                        <span class="offer">%{{$body['specifications']['price']['discount']}} تخفیف </span>
                                                    </div>
                                                @endif
                                            </h3>
                                        </div>
                                        <div class="part_two">
                                            <h4>
                                                @if($body['specifications']['price']['discount'] > 0)
                                                    {{number_format($body['specifications']['price']['price_after_discount'])}}
                                                @else
                                                    {{number_format($body['specifications']['price']['base_price'])}}
                                                @endif
                                                <span class="font-s-13">تومان</span>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($body['specifications']['properties']))
                                    <div class="col-12">
                                        <div class="product_specifications">
                                            <ul class="list-group">
                                                <div class="row">
                                                    @foreach($body['specifications']['properties'] as $property)
                                                        <div class="col-12 col-md-6 mb-3">
                                                            <li class="list-group-item">
                                                                <i class="tio pe-7s-angle-left"></i>
                                                                {{$property['key']}}
                                                                <span class="c-blue"> {{$property['value']}} </span>
                                                            </li>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-12">
                                    <section class="faq_two_inner py-0 mt-3 mb-1">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-12">
                                                <!-- block Collapse -->
                                                <div class="faq_section faq_demo3 faq_with_icon">
                                                    <div class="block_faq">
                                                        <div class="accordion" id="accordionExample2">
                                                            <div class="card">
                                                                <div class="card-header" id="headingSix">
                                                                    <h3 class="mb-0">
                                                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                                                            <i class="tio link"></i>
                                                                            توضیحات اجمالی
                                                                        </button>
                                                                    </h3>
                                                                </div>

                                                                <div id="collapse2" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample2">
                                                                    <div class="card-body">
                                                                        <p>
                                                                            {{$body['description']}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-12">
                                    <section class="product_promo">
                                        <div class="swip__stories ">
                                            <!-- Swiper -->
                                            <div class="swiper-container product_promo_slides">
                                                <div class="swiper-wrapper">
                                                    @foreach($body['catalog'] as $item)
                                                        <div class="swiper-slide">
                                                            <div class="item">
                                                                <div class="img__nature">
                                                                    <img src="{{$item['primary_file_name']}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>

                                                <!-- Add Arrows -->
                                                <div class="swiper-button-next">
                                                    <i class="tio pe-7s-angle-right"></i>
                                                </div>
                                                <div class="swiper-button-prev">
                                                    <i class="tio pe-7s-angle-left"></i>
                                                </div>

                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- End. -->

        <!-- Start product promo -->
        {{--<section class="product_promo padding-t-3 padding-b-5">
            <div class="container">

            </div>
        </section>--}}
        <!-- End. product promo -->

        <!-- Start product technical description -->
        {{--<section class="faq_two_inner py-0 mt-0 margin-b-6">
            <div class="container">

                <div class="faq_one_inner py-0 my-0">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <!-- block Collapse -->
                            <div class="faq_section faq_demo3 faq_with_icon">
                                <div class="block_faq">
                                    <div class="accordion" id="accordionExample2">
                                        <div class="card">
                                            <div class="card-header" id="headingSix">
                                                <h3 class="mb-0">
                                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                                        <i class="tio link"></i>
                                                        توضیحات اجمالی
                                                    </button>
                                                </h3>
                                            </div>

                                            <div id="collapse2" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample2">
                                                <div class="card-body">
                                                    <p>
                                                        {{$body['description']}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>--}}
        <!-- End. product technical description -->

    </main>


@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

