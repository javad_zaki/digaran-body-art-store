@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />

@endsection


{{--@section('category')
    <nav id="chapternav" class="chapternav" data-analytics-region="family browser" aria-label="Mac Family of products">
        <div class="chapternav-wrapper">
            <ul class="chapternav-items">
                @foreach($body['categories'] as $category)
                    <li class="chapternav-item chapternav-item-macbook-air">
                        <a class="chapternav-link" href="{{url('tattoo/store?category='.$category['id'])}}">
                            <figure class="chapternav-icon" --}}{{--style="background-image: url({{$category['image']}})"--}}{{--></figure>
                            <span class="chapternav-label" role="text">{{$category['name']}}</span>
                        </a>
                    </li>
                @endforeach
                <li class="chapternav-item chapternav-item-macbook-air">
                    <a class="chapternav-link" href="{{url('tattoo/store')}}">
                        <figure class="chapternav-icon" --}}{{--style="background-image: url({{asset('/img/layout/icon/categories/more.svg')}})"--}}{{--></figure>
                        <span class="chapternav-label" role="text">همه محصول ها</span>
                    </a>
                </li>
            </ul>
            <div class="chapternav-paddles">
                <button class="chapternav-paddle chapternav-paddle-left d-md-none" aria-hidden="true"></button>
                --}}{{--<button class="chapternav-paddle chapternav-paddle-right" aria-hidden="true" disabled></button>--}}{{--
            </div>
        </div>
    </nav>
@endsection--}}

@section('body')

    <main>

        {{--<section class="pt_banner_inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="category_products margin-t-6 margin-b-6">
                            <div class="container-fluid">
                                <div class="row text-center">
                                    <div class="col-12 aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
                                        @foreach($body['categories'] as $category)

                                            <a href="{{url('tattoo/store?category='.$category['id'])}}">
                                                <div class="item">
                                                    <span class="tio">
                                                       <img src="{{$category['image']}}">
                                                    </span>
                                                    <div class="content">
                                                        <h3>{{$category['name']}}</h3>
                                                    </div>
                                                </div>
                                            </a>

                                        @endforeach
                                        <a href="{{url('tattoo/store')}}">
                                            <div class="item">
                                                <span class="tio">
                                                    <img src="{{asset('/img/layout/icon/categories/more.svg')}}">
                                                </span>
                                                <div class="content">
                                                    <h3>همه</h3>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}

        <!-- Start search bar -->
        {{--<section class="margin-t-10">
            <div class="container-fluid">
                <div class="filter_form margin-t-1 aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                    <form class="row" method="GET" action="{{ url('store') }}">
                        <div class="col-12 col-lg-6">
                            <div class="simple_search">
                                <div class="form-group">
                                    <div class="input_group">
                                        <input type="text" name="expr" class="form-control" placeholder="عبارت مورد نظر را جستجو کنید">
                                        <i class="pe-7s-search tio"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="form-group">
                                <select class="form-control custom-select" name="category">
                                    <option value="">دسته بندی</option>
                                    @if(!$body['categories']->isEmpty())
                                        @foreach($body['categories'] as $category)
                                            <option value="{{$category['id']}}">{{$category['name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <button type="submit" class="btn btn_md_primary btn-block scale border-0 sweep_letter sweep_top bg-blue c-white rounded-8">
                                <div class="inside_item">
                                    <span data-hover="جستجو کن">جستجو</span>
                                </div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>--}}
        <!-- End search bar -->

        {{--<div class="container-fluid margin-my-7">
            <div class="dividar_line"></div>
        </div>--}}

        <!-- Start Products -->
        <section class="product_masonry">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 margin-b-12 margin-t-5">
                        @if(!$body['products']->isEmpty())
                            <div class="row">
                            @foreach($body['products'] as $product)
                                <div class="col-12 col-sm-2 col-md-4">
                                    <a href="{{route("{$body['prefix']}.store.product", ["sku" => $product['sku']])}}">
                                    <div class="card">
                                        <div class="cover_link">
                                            <img class="main_img" src="{{$product['image']}}" alt="...">
                                            <div class="detail">
                                                <div class="media">
                                                    <div class="media-body my-auto">
                                                        <div class="txt">
                                                           {{-- <span>{{$product['category']}}</span>--}}
                                                            <h4>{{$product['name']}}</h4>
                                                            @if($product['discount'] > 0)
                                                                <h3 class="line-through c-gray">{{number_format($product['base_price'])}} ریال </h3>
                                                                <h3 class="c-blue">{{number_format($product['price_after_discount'])}} ریال </h3>
                                                            @else
                                                                <h3 class="c-blue">{{number_format($product['base_price'])}} ریال </h3>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<div class="action">
                                                <div class="media">
                                                    <div class="media-body my-auto">
                                                        <span>مشاهده</span>
                                                    </div>
                                                </div>
                                            </div>--}}
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            @endforeach

                            @if($body['pagination']['last_page'] > 1)
                                <div class="col-12">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination default margin-t-5">

                                            @if($body['pagination']['current_page'] > 1)
                                            <li class="page-item">
                                                <a class="page-link" href="{{url('/'.session()->get('prefix').'/store?category='.$body['category'])}}" aria-label="Previous">
                                                    صفحه اول
                                                </a>
                                            </li>
                                            @endif

                                            @if($body['pagination']['current_page'] > 1)
                                                <li class="page-item">
                                                    <a class="page-link" href="{{url('/'.session()->get('prefix').'/store?category='.$body['category'].'&page='.($body['pagination']['current_page']-1))}}" aria-label="Previous">
                                                        <i class="pe-7s-angle-right tio"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            <li class="page-item"><a class="page-link active" href="{{url('/'.session()->get('prefix').'/store?category='.$body['category'].'&page='.$body['pagination']['current_page'])}}">{{$body['pagination']['current_page']}}</a></li>
                                            @if($body['pagination']['current_page'] < $body['pagination']['last_page'])
                                            <li class="page-item">
                                                <a class="page-link" href="{{url('/'.session()->get('prefix').'/store?category='.$body['category'].'&page='.($body['pagination']['current_page']+1))}}" aria-label="Next">
                                                    <i class="pe-7s-angle-left tio"></i>
                                                </a>
                                            </li>
                                            @endif

                                            @if($body['pagination']['current_page'] < $body['pagination']['last_page'])
                                            <li class="page-item">
                                                <a class="page-link" href="{{url('/'.session()->get('prefix').'/store?category='.$body['category'].'&page='.$body['pagination']['last_page'])}}" aria-label="Previous">
                                                    صفحه آخر {{'('.$body['pagination']['last_page'] .')'}}
                                                </a>
                                            </li>
                                            @endif

                                        </ul>
                                    </nav>
                                </div>
                            @endif

                            </div>
                        @else
                            <div class="row">
                                <div class="col-12 col-lg-6 offset-lg-3">
                                    <div class="alert alert-warning fade show" role="alert">
                                        در حال حاضر محصولی در این دسته بندی وجود ندارد.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- End Products -->

    </main>


@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

