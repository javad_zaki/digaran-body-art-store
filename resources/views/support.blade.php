@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="{{ asset('/css/revolution/settings.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/revolution/layers.css') }}" type="text/css" />
@endsection

@section('body')
    <main>
        <!-- Start section_contact_four -->
        <section class="section_contact_five contact_six margin-t-5 padding-t-7 margin-b-15">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="title_sections_inner">
                            <h2>پشتیبانی و تماس</h2>
                        </div>
                        <div class="information_agency d-md-flex">
                            <div class="item_data mr-5">
                                <p>شماره تماس</p>
                                <a href="tel:+18091206705">{{$body['support']['support_number']}}</a>
                            </div>
                            <div class="item_data">
                                <p>زمان دسترسی</p>
                                <div>{{$body['support']['availability']}}</div>
                            </div>
                        </div>

                        <div class="information_agency margin-t-4">
                            <div class="item_data">
                                <p>ایمیل </p>
                                <a href="mailto:info@agency.com">{{$body['support']['support_email']}}</a>
                            </div>
                        </div>

                        {{--<h3 class="font-s-16 font-w-500 c-gray margin-t-4">نشانی</h3>
                        <p class="font-s-16">خوزستان - اهواز - خیابان شهید وهابی - روبروی خیابان 6 کیانپارس - مجتمع مهندسان - طبقه 1 - واحد 2</p>--}}

                        <div class="scoail__media mb-5">
                            @foreach($body['support']['channels'] as $item)
                                @switch($item['channel'])
                                    @case('instagram')
                                    <a href="https://www.instagram.com/{{$item['username']}}" target="_blank">
                                        <div class="instagram">
                                            <i class="socicon-instagram tio"></i>
                                        </div>
                                    </a>
                                    @break
                                    @case('facebook')
                                    <a href="https://www.facebook.com/{{$item['username']}}" target="_blank">
                                        <div class="facebook">
                                            <i class="socicon-facebook tio"></i>
                                        </div>
                                    </a>
                                    @break
                                    @case('youtube')
                                    <a href="https://www.youtube.com/{{$item['username']}}" target="_blank">
                                        <div class="youtube">
                                            <i class="socicon-youtube tio"></i>
                                        </div>
                                    </a>
                                    @break
                                    @case('pinterest')
                                    <a href="https://www.pinterest.com/{{$item['username']}}" target="_blank">
                                        <div class="pinterest">
                                            <i class="socicon-pinterest tio"></i>
                                        </div>
                                    </a>
                                    @break
                                    @case('whatsapp')
                                    <a href="https://www.wa.me/{{$item['username']}}" target="_blank">
                                        <div class="whatsapp">
                                            <i class="socicon-whatsapp tio"></i>
                                        </div>
                                    </a>
                                    @break
                                @endswitch
                            @endforeach
                        </div>

                    </div>
                    <div class="col-12 col-lg-7 offset-lg-1">
                        <div class="title_sections_inner">
                            <h2>سوال های متداول</h2>
                            <p>سوال هایی در مورد نحوه عضویت، خرید از فروشگاه و آموزش های آکادمی ما رو اینجا می تونید ببنید.</p>
                        </div>
                        <!-- block Collapse -->
                        <div class="faq_section faq_demo3 faq_with_icon">
                            <div class="block_faq">
                                <div class="accordion" id="accordionExample">

                                    @foreach($body['support']['faq'] as $key => $item)
                                        <div class="card" data-aos="fade-up" data-aos-delay="0">
                                            <div class="card-header {{($key == 0) ? 'active' : ''}}" id="heading{{$key}}">
                                                <h3 class="mb-0">
                                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                                            data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                                                        <i class="tio link"></i>
                                                        {{$item['question']}}
                                                    </button>
                                                </h3>
                                            </div>

                                            <div id="collapse{{$key}}" class="collapse {{($key == 0) ? 'show' : ''}}" aria-labelledby="heading{{$key}}"
                                                 data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <p class="mb-0">
                                                        {!! nl2br(e($item['answer'])) !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- End. section_contact_four -->

    </main>
@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.viewport.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/animsition.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/svg4everybody.legacy.min.js') }}"></script>
@endsection
