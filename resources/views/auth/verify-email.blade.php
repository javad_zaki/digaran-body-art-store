@extends('layouts.app')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Start form_signup_onek -->
    <section class="form_signup_one signup_two">
        <div class="container margin-t-10">
            <div class="row">
                <div class="col-lg-5 offset-1 my-auto">
                    <div class="item_brand margin-b-3">
                        <a href="">
                            <img class="mr-2" src="{{asset('/img/layout/blacklogo.png')}}" alt="">
                        </a>
                    </div>
                    <div class="title_sections margin-b-5">
                        <h2 class="c-black mb-1">فعال سازی ایمیل</h2>
                        <p class="c-gray">لینک فعال سازی به ایمیل شما ارسال شده است. در صورتی که آن را دریافت نکرده اید، درخواست ارسال مجدد لینک را بزنید.</p>
                    </div>
                </div>
                <div class="col-md-7 col-lg-5 ml-lg-auto margin-t-8">
                    <div class="item_group">
                        @if (session('status') == 'verification-link-sent')
                            <div class="alert alert-primary alert-dismissible fade show mb-2" role="alert">
                                <p class="mb-0 font-small-3">یک ایمیل اعتبارسنجی جدید برای شما ارسال شد.</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">
                                        <i class="feather icon-x-circle"></i>
                                    </span>
                                </button>
                            </div>
                        @endif
                        <form action="{{ route('verification.send') }}" method="POST" class="row">
                            @csrf
                            <div class="col-12">
                                <button type="submit" class="btn w-100 margin-t-3 btn_account bg-blue c-white rounded-8">
                                    ارسال لینک
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End.form_signup_one -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

