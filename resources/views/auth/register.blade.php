@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')
    <!-- Start form_signup_onek -->
    <section class="signup_full">
    <div class="form_signup_one margin-t-10 margin-b-15">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 margin-t-8">
                    <div class="item_group">
                        <form action="{{ route('register') }}" method="POST" class="row">
                            @csrf

                            <div class="col-12">
                                <div class="title_sign">
                                    <h2>ایجاد حساب کاربری</h2>
                                    <p>قبلا حساب کاربری ایجاد کرده ایید? <a href="{{ route('login') }}" class="c-blue">ورود به حساب کاربری</a></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>کد اعتبارسنجی</label>
                                    <input type="text" name="code" class="form-control" value="{{ old('code') }}" autofocus placeholder="کد اعتبارسنجی را وارد نمایید">
                                    {!! $errors->first('code', '<span class="text-warning font-small-2">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group --password" id="show_hide_password">
                                    <label>رمز عبور</label>
                                    <div class="input-group">
                                        <input type="password" name="password" class="form-control" data-toggle="password" placeholder="8+ کارکتر"/>
                                        <div class="input-group-prepend hide_show">
                                            <a href=""><span class="input-group-text tio pe-7s-shield"></span></a>
                                        </div>
                                    </div>
                                    {!! $errors->first('password', '<span class="text-warning font-small-2">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group --password" id="show_hide_password">
                                    <label>تایید رمز عبور</label>
                                    <div class="input-group">
                                        <input type="password" name="password_confirmation" class="form-control" data-toggle="password_confirmation"  placeholder="8+ کارکتر"/>
                                        <div class="input-group-prepend hide_show">
                                            <a href=""><span class="input-group-text tio pe-7s-shield"></span></a>
                                        </div>
                                    </div>
                                    {!! $errors->first('password_confirmation', '<span class="text-warning font-small-2">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn w-100 margin-t-3 btn_account bg-blue c-white rounded-8">
                                    ایجاد حساب
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <!-- End.form_signup_one -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

