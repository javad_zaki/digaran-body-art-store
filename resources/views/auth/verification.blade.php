@extends('layouts.guest')

@section('title')
    <title>اعتبارسنجی شماره موبایل</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')
    <!-- Start form_signup_onek -->
    <section class="signup_full">
    <div class="form_signup_one margin-t-10 margin-b-15">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3 margin-t-8">
                    <div class="item_group">
                        <form action="{{ route('sendVerificationCode') }}" method="POST" class="row">
                            @csrf
                            <input type="hidden" name="unique" value="1">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>شماره موبایل</label>
                                    <input type="text" name="mobile" class="form-control" value="{{ old('mobile') }}" autofocus placeholder="09+">
                                    {!! $errors->first('mobile', '<span class="text-warning font-small-2">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn w-100 margin-t-3 btn_account bg-blue c-white rounded-8">
                                    ارسال کد
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <!-- End.form_signup_one -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

