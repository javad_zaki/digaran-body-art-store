@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <section class="signup_full">
        <div class="form_signup_one margin-t-3 margin-b-15">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 margin-t-8">
                        <div class="item_group">
                            <form action="{{ route('login') }}" method="POST" class="row">
                                @csrf

                                <div class="col-12">
                                    <div class="title_sign">
                                        <h2>ورود به حساب کاربری</h2>
                                        <p>کاربر جدید هستید? <a href="{{ route('verification') }}" class="c-blue">ایجاد حساب کاربری</a></p>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label>شماره موبایل</label>
                                        <input type="text" name="mobile" class="form-control ltr" value="{{ old('mobile') }}" autofocus placeholder="09">
                                        {!! $errors->first('mobile', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group --password" id="show_hide_password">
                                        <label>رمز عبور</label>
                                        <div class="input-group">
                                            <input type="password" name="password" class="form-control" data-toggle="password" placeholder="8+ کارکتر">
                                            <div class="input-group-prepend hide_show">
                                                <a href=""><span class="input-group-text tio pe-7s-shield"></span></a>
                                            </div>
                                        </div>
                                        {!! $errors->first('password', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="block mt-2">
                                        <label for="remember_me" class="inline-flex items-center">
                                            <input id="remember_me" type="checkbox" class="rounded" name="remember">
                                            <span class="my-auto mx-auto font-s-15 text-gray-600">مرا به خاطر بسپار</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-12">
                                    @if (Route::has('password.request'))
                                        <a class="" href="{{ route('password.request') }}">
                                            رمز عبور خود را فراموش کرده اید؟
                                        </a>
                                    @endif
                                    <button type="submit" class="btn btn-lg w-100 margin-t-3 btn_account bg-blue c-white rounded-8">
                                        ورود به حساب
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    {{--<div class="col-lg-5 offset-1 my-auto">
                 <div class="item_brand margin-b-3">
                     <a href="">
                         <img class="mr-2" src="{{asset('/img/layout/blacklogo.png')}}" alt="">
                     </a>
                 </div>
                 <div class="title_sections margin-b-5">
                     <h2 class="c-black mb-1">ورود به حساب کاربری</h2>
                     <p class="c-gray">کاربر جدید هستید؟ <a href="{{ route('verification') }}" class="c-blue"> ایجاد حساب کاربری </a></p>
                     <p class="c-gray">برو به  <a href="{{ route('index') }}" class="c-blue"> صفحه اصلی </a></p>
                 </div>
                 <div class="list__point">
                     <div class="item media">
                         <div class="icob">
                             <i class="tio pe-7s-user"></i>
                         </div>
                         <div class="media-body my-auto">
                             <p>مدیریت اطلاعات شخصی و آدرس</p>
                         </div>
                     </div>
                     <div class="item media">
                         <div class="icob">
                             <i class="tio pe-7s-box2"></i>
                         </div>
                         <div class="media-body my-auto">
                             <p>مدیریت سفارش ها و محصولات مورد علاقه</p>
                         </div>
                     </div>
                     <div class="item media">
                         <div class="icob">
                             <i class="tio pe-7s-play"></i>
                         </div>
                         <div class="media-body my-auto">
                             <p>مدیریت آموزش های ذخیره شده</p>
                         </div>
                     </div>
                 </div>
             </div>--}}

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

