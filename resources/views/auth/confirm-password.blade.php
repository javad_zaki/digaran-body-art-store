@extends('layouts.app')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')
    <!-- Start form_signup_onek -->
    <section class="form_signup_one signup_two">
        <div class="container margin-t-10">
            <div class="row">
                <div class="col-lg-5 offset-1 my-auto">

                    <div class="item_brand margin-b-3">
                        <a href="">
                            <img class="mr-2" src="{{asset('/img/layout/blacklogo.png')}}" alt="">
                        </a>
                    </div>

                    <div class="title_sections margin-b-5">
                        <h2 class="c-black mb-1">تایید رمز عبور</h2>
                        <p class="c-gray">لطفا قبل از اینکه ادامه دهید، رمز عبور خود را تایید نمایید.</p>
                    </div>

                </div>
                <div class="col-md-7 col-lg-5 ml-lg-auto">
                    <div class="item_group">
                        <form action="{{ route('password.confirm') }}" method="POST" class="row">
                            @csrf
                            <div class="col-md-12">
                                <div class="form-group --password" id="show_hide_password">
                                    <label>رمز عبور</label>
                                    <div class="input-group">
                                        <input type="password" name="password" class="form-control" data-toggle="password" placeholder="رمز عبور ترکیبی از اعداد و حروف باشد"/>
                                        <div class="input-group-prepend hide_show">
                                            <a href=""><span class="input-group-text tio hidden_outlined"></span></a>
                                        </div>
                                    </div>
                                    {!! $errors->first('password', '<span class="text-warning font-small-2">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn w-100 margin-t-3 btn_account bg-blue c-white rounded-8">
                                    تایید
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End.form_signup_one -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

