@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="{{ asset('/css/revolution/settings.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/revolution/layers.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <!-- Start Slider -->
        <section class="main-slider">
            <div class="revolution-slider">
                <div class="revolution-slider__slider rev rev_slider fullwidthabanner false" data-version="5.4.6" style="display:none;">
                    <ul>
                        @if(!$body['slides']->isEmpty())
                            @foreach($body['slides'] as $slide)
                                <li data-transition="fade">
                                    <img alt=""
                                         class="rev-slidebg rev-slidebg"
                                         data-bgparallax="off"
                                         data-bgposition="center center"
                                         data-blurend="0"
                                         data-blurstart="20"
                                         data-duration="2000"
                                         data-ease="Power3.easeInOut"
                                         data-kenburns="on"
                                         data-lazyload="{{$slide['image']}}"
                                         data-no-retina="data-no-retina"
                                         data-offsetend="0 0"
                                         data-offsetstart="0 0"
                                         data-rotateend="0"
                                         data-rotatestart="0"
                                         data-scaleend="0"
                                         data-scalestart="0"
                                         src=""/>
                                    <a href="{{$slide['link']}}"> <div class="main-slide main-slide main-slide_size_large">
                                        <div class="main-slide__container container">
                                            <div class="main-slide__slide-inner">
                                                <div class="main-slide__link-wrapper">
                                                    <div class="main-slide__link-inner tp-caption"
                                                         data-frames='[{"delay":"+190","speed":500,"sfxcolor":"#ffffff","sfx_effect":"","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"opacity:0;","ease":"Power4.easeOut"}]'
                                                         data-height="none"
                                                         data-hoffset="['0','0','0','0']"
                                                         data-responsive="off"
                                                         data-responsive_offset="off"
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-type="text"
                                                         data-voffset="['0','0','0','0']"
                                                         data-whitespace="nowrap"
                                                         data-width="none"
                                                         data-x="['left','left','left','left']"
                                                         data-y="['top','top','top','top']">
                                                        {{--<a href="" class="btn btn-sm scale btn-video">
                                                            {{$slide['title']}}
                                                        </a>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div></a>
                                </li>
                            @endforeach
                        @else
                            <li data-transition="fade">
                                <img alt=""
                                     class="rev-slidebg rev-slidebg"
                                     data-bgparallax="off"
                                     data-bgposition="center center"
                                     data-blurend="0"
                                     data-blurstart="20"
                                     data-duration="2000"
                                     data-ease="Power3.easeInOut"
                                     data-kenburns="on"
                                     data-lazyload="{{asset('img/default/placeholder-image.jpg')}}"
                                     data-no-retina="data-no-retina"
                                     data-offsetend="0 0"
                                     data-offsetstart="0 0"
                                     data-rotateend="0"
                                     data-rotatestart="0"
                                     data-scaleend="0"
                                     data-scalestart="0"
                                     src=""/>
                                <a href=""> <div class="main-slide main-slide main-slide_size_large">
                                    <div class="main-slide__container container">
                                        <div class="main-slide__slide-inner">
                                            <div class="main-slide__link-wrapper">
                                                <div class="main-slide__link-inner tp-caption"
                                                     data-frames='[{"delay":"+190","speed":500,"sfxcolor":"#ffffff","sfx_effect":"","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"opacity:0;","ease":"Power4.easeOut"}]'
                                                     data-height="none"
                                                     data-hoffset="['0','0','0','0']"
                                                     data-responsive="off"
                                                     data-responsive_offset="off"
                                                     data-textAlign="['inherit','inherit','inherit','inherit']"
                                                     data-type="text"
                                                     data-voffset="['0','0','0','0']"
                                                     data-whitespace="nowrap"
                                                     data-width="none"
                                                     data-x="['left','left','left','left']"
                                                     data-y="['top','top','top','top']">
                                                    {{--<a href="{{url('/')}}" class="btn btn-sm scale btn-video">
                                                        دیگران
                                                    </a>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div></a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </section>
        <!-- End Slider -->

        <!-- Start Promo -->
        <section class="serv_app padding-t-5 padding-b-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    @if(!$body['promos']->isEmpty())
                        @foreach($body['promos'] as $promo)
                            <div class="col-lg-6 mb-4 mb-lg-0">
                                <a href="{{$promo['link']}}">
                                    <div class="amo_pic" style="background-image: url({{$promo['image']}})">
                                        <img src="{{$promo['image']}}" style="display: none"/>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                            <a href="{{url('/')}}">
                                <div class="amo_pic" style="background-image: url({{asset('img/default/placeholder-image.jpg')}})">
                                    <img src="{{asset('img/default/placeholder-image.jpg')}}" style="display: none"/>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                            <a href="{{url('/')}}">
                                <div class="amo_pic" style="background-image: url({{asset('img/default/placeholder-image.jpg')}})">
                                    <img src="{{asset('img/default/placeholder-image.jpg')}}" style="display: none"/>
                                </div>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </section>
        <!-- End Promo -->

        <!-- Start featured products -->
        <section class="section__stories feature_strories padding-t-3 padding-b-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="swip__stories">
                            <div class="swiper-container feature_strories">
                                <div class="title_sections">
                                    <h4>محصول های ویژه</h4>
                                </div>
                                @if(!$body['products']->isEmpty())
                                    <div class="swiper-wrapper">
                                    @foreach($body['products'] as $product)
                                        <div class="swiper-slide">
                                            <a href="{{route("{$body['prefix']}.store.product", ["sku" => $product['sku']])}}" class="item">
                                                <div class="img__nature">
                                                    <img src="{{$product['image']}}">
                                                </div>
                                                <div class="inf__txt">
                                                    <span>{{$product['category']}}</span>
                                                    <h3>{{$product['name']}}</h3>
                                                    @if($product['discount'] > 0)
                                                        <h4 class="line-through c-red">  {{number_format($product['base_price'])}} ریال </h4>
                                                        <h4>{{number_format($product['price_after_discount'])}} ریال </h4>
                                                    @else
                                                        <h4> {{number_format($product['base_price'])}} ریال </h4>
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                    </div>
                                @else
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a href="#" class="item">
                                                <div class="img__nature">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="#" class="item">
                                                <div class="img__nature">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="#" class="item">
                                                <div class="img__nature">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}">
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endif

                                <div class="swiper-button-next">
                                    <i class="tio pe-7s-angle-right"></i>
                                </div>
                                <div class="swiper-button-prev">
                                    <i class="tio pe-7s-angle-left"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-12 text-center margin-t-5">
                    <a href="{{route("{$body['prefix']}.store")}}" class="btn btn_md_primary rounded-7 border-1 c-dark text-center">
                        محصول های بیشتر
                    </a>
                </div>--}}
            </div>
        </section>
        <!-- End. featured products -->

        <!-- Start Store call to action -->
        {{--<section class="creative_box_contact padding-t-5">
            <div class="container-fluid">
                <div class="content">
                    <div class="row text-center">
                        <div class="col-md-6 col-lg-4 mb-3 mb-lg-0 padding-r-5">
                            <div class="items_serv sevice_block aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
                                <div class="icon--top">
                                    <img src="{{asset('/img/layout/icon/barcode.svg')}}" alt="">
                                </div>
                                <div class="txt c-white">
                                    <h6>کالای اصل</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 mb-3 mb-lg-0">
                            <div class="items_serv sevice_block aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                                <div class="icon--top">
                                    <img src="{{asset('/img/layout/icon/returning.svg')}}" alt="">
                                </div>
                                <div class="txt c-white">
                                    <h6>ضمانت بازگشت</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="items_serv sevice_block aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                                <div class="icon--top">
                                    <img src="{{asset('/img/layout/icon/express.svg')}}" alt="">
                                </div>
                                <div class="txt c-white">
                                    <h6>تحویل اکسپرس</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row text-center margin-t-5">
                        <div class="col-12">
                            <div class="link_state">
                                <a href="{{route("{$body['prefix']}.store")}}" class="btn btn-sm scale rounded-pill btn-video">شروع خرید</a>
                                <a href="{{route("user")}}" class="btn btn_md_primary border-1 bg-transparent sweep_letter sweep_top c-white rounded-pill ">
                                    <div class="inside_item">
                                        <span data-hover="حساب کاربری">حساب کاربری</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}
        <!-- End. Store call to action -->

        <!-- Start featured courses -->
      {{--  <section class="section__stories blog_slider padding-t-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="swip__stories">
                            <div class="swiper-container blog-slider">
                                <div class="title_sections_inner">
                                    <h4>آموزش های ویژه</h4>
                                </div>
                                <div class="swiper-wrapper">

                                    @if(!$body['courses']->isEmpty())

                                        @foreach($body['courses'] as $course)
                                            <div class="swiper-slide">
                                                <div class="grid_blog_avatar">
                                                    <div class="cover_blog">
                                                        <img src="{{$course['image']}}" alt="">
                                                        <div class="icon_played">
                                                            <a href="{{route("{$body['prefix']}.academy.course", ["code" => $course['code']])}}" class="btn btn_video">
                                                                <div class="scale rounded-circle b play_video">
                                                                    <i class="tio pe-7s-link"></i>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="body_blog">
                                                        <a href="{{url('/academy/course/'.$course['code'])}}">
                                                            <div class="person media">
                                                                <div class="media-body">
                                                                    <div class="txt">
                                                                        <h3>{{$course['title']}}</h3>
                                                                        <time><span class="pe-7s-timer"></span> {{gmdate("H:i:s",$course['duration'])}} </time>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    @else
                                        <div class="swiper-slide">
                                            <div class="grid_blog_avatar">
                                                <div class="cover_blog">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}" alt="">
                                                    <div class="icon_played">
                                                        <button type="button" class="btn btn_video">
                                                            <div class="scale rounded-circle b play_video">
                                                                <i class="tio pe-7s-play"></i>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="grid_blog_avatar">
                                                <div class="cover_blog">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}" alt="">
                                                    <div class="icon_played">
                                                        <button type="button" class="btn btn_video">
                                                            <div class="scale rounded-circle b play_video">
                                                                <i class="tio pe-7s-play"></i>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="grid_blog_avatar">
                                                <div class="cover_blog">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}" alt="">
                                                    <div class="icon_played">
                                                        <button type="button" class="btn btn_video">
                                                            <div class="scale rounded-circle b play_video">
                                                                <i class="tio pe-7s-play"></i>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </div>
                                <div class="swiper-button-next">
                                    <i class="tio pe-7s-angle-left"></i>
                                </div>
                                <div class="swiper-button-prev">
                                    <i class="tio pe-7s-angle-right"></i>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-12 text-center">
                    <a href="{{route("{$body['prefix']}.academy")}}" class="btn btn_md_primary rounded-7 border-1 c-dark text-center">
                        آموزش های بیشتر
                    </a>
                </div>

            </div>
        </section>--}}
        <!-- End featured courses -->


        <!-- Start. Academy call to action -->
        {{--<section class="creative_box_contact padding-t-10">
            <div class="container-fluid">
                <div class="content">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-6 col-lg-6 mb-3 mb-lg-0 padding-r-5">
                            <div class="title_sections_inner margin-b-4">
                                <h5 class="c-white aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">آموزش های دیگران رو دنبال کنید</h5>
                            </div>
                            <a href="{{route("{$body['prefix']}.academy")}}" class="btn btn-sm scale rounded-pill btn-video">آکادمی</a>
                            <a href="{{route("user")}}" class="btn btn_md_primary border-1 bg-transparent sweep_letter sweep_top c-white rounded-pill ">
                                <div class="inside_item">
                                    <span data-hover="حساب کاربری">حساب کاربری</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}
        <!-- End. Academy call to action -->


        <!-- Start Artists -->
        <section class="section__stories artist_slider padding-t-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="swip__stories">
                            <div class="swiper-container team_overlay_style artist-slider">
                                <div class="title_sections_inner">
                                    <h4>هنرمندان</h4>
                                </div>
                                <div class="swiper-wrapper">
                                    @if(!$body['artists']->isEmpty())
                                        @foreach($body['artists'] as $artist)
                                            <div class="swiper-slide">
                                                <div class="item_group">
                                                    <div class="image_ps">
                                                        <img src="{{$artist['image']}}" alt="">
                                                    </div>
                                                    <div class="share_soisal">
                                                        @foreach($artist['social'] as $item)
                                                            @switch($item['channel'])
                                                                @case('instagram')
                                                                <a href="https://www.instagram.com/{{$item['username']}}" target="_blank">
                                                                    <i class="socicon-instagram tio"></i>
                                                                </a>
                                                                @break
                                                                @case('facebook')
                                                                <a href="https://www.facebook.com/{{$item['username']}}" target="_blank">
                                                                    <i class="socicon-facebook tio"></i>
                                                                </a>
                                                                @break
                                                                @case('twitter')
                                                                <a href="https://www.twitter.com/{{$item['username']}}" target="_blank">
                                                                    <i class="socicon-twitter tio"></i>
                                                                </a>
                                                                @break
                                                            @endswitch
                                                        @endforeach
                                                    </div>
                                                    <div class="content_txt">
                                                        <h3>{{$artist['name']}}</h3>
                                                        <p>{{$artist['biography']}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="swiper-slide">
                                            <div class="item_group">
                                                <div class="image_ps">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item_group">
                                                <div class="image_ps">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item_group">
                                                <div class="image_ps">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="item_group">
                                                <div class="image_ps">
                                                    <img src="{{asset('/img/default/placeholder-image.jpg')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </div>
                                <div class="swiper-button-next">
                                    <i class="tio pe-7s-angle-left"></i>
                                </div>
                                <div class="swiper-button-prev">
                                    <i class="tio pe-7s-angle-right"></i>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End. Artists -->

    </main>


@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.viewport.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/animsition.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/svg4everybody.legacy.min.js') }}"></script>
@endsection

@section('tail-script')
    <script type="text/javascript" src="{{ asset('/js/revolution/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.actions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.migration.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/revolution/extensions/revolution.extension.video.min.js') }}"></script>
@endsection
