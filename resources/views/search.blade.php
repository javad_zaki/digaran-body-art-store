@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="{{ asset('/css/revolution/settings.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/revolution/layers.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <!-- Start featured products -->
        <section class="section__stories feature_strories padding-t-3 padding-b-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="swip__stories">
                            <div class="swiper-container feature_strories">
                                <div class="title_sections">
                                    <h4>نتایج جستجو</h4>
                                </div>
                                @if(!$body['products']->isEmpty())
                                    <div class="swiper-wrapper">
                                    @foreach($body['products'] as $product)
                                        <div class="swiper-slide">
                                            <a href="{{route("{$product['prefix']}.store.product", ["sku" => $product['sku']])}}" class="item">
                                                <div class="img__nature">
                                                    <img src="{{$product['image']}}">
                                                </div>
                                                <div class="inf__txt">
                                                    <span>{{$product['category']}}</span>
                                                    <h3>{{$product['name']}}</h3>
                                                    <h4 class="line-through c-red">{{number_format($product['base_price'])}}</h4>
                                                    <h4>{{number_format($product['price_after_discount'])}}</h4>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-12 col-md-6 offset-md-3">
                                            <div class="alert alert-warning fade show" role="alert">
                                                محصول مورد نظر پیدا نشد.
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="swiper-button-next">
                                    <i class="tio pe-7s-angle-right"></i>
                                </div>
                                <div class="swiper-button-prev">
                                    <i class="tio pe-7s-angle-left"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               {{-- <div class="col-12 text-center mb-5 mt-5">
                    <a href="{{route('tattoo.store')}}" class="btn btn_md_primary rounded-pill border-1 c-dark text-center">
                        محصول های تاتو
                    </a>
                    <a href="{{route('piercing.store')}}" class="btn btn_md_primary rounded-pill border-1 c-dark text-center">
                        محصول های پیرسینگ
                    </a>
                </div>--}}
            </div>
        </section>
        <!-- End. featured products -->


        <!-- Start featured courses -->
        {{--<section class="section__stories blog_slider padding-t-5">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="swip__stories">
                            <div class="swiper-container blog-slider">
                                <div class="title_sections_inner">
                                    <h4>آموزش های ویژه</h4>
                                </div>

                                    @if(!$body['courses']->isEmpty())
                                    <div class="swiper-wrapper">
                                        @foreach($body['courses'] as $course)
                                            <div class="swiper-slide">
                                                <div class="grid_blog_avatar">
                                                    <div class="cover_blog">
                                                        <img src="{{$course['image']}}" alt="">
                                                        <div class="icon_played">
                                                            <a href="{{route("{$course['prefix']}.academy.course", ["code" => $course['code']])}}" class="btn btn_video">
                                                                <div class="scale rounded-circle b play_video">
                                                                    <i class="tio pe-7s-link"></i>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="body_blog">
                                                        <a href="{{url('/academy/course/'.$course['code'])}}">
                                                            <div class="person media">
                                                                <div class="media-body">
                                                                    <div class="txt">
                                                                        <h3>{{$course['title']}}</h3>
                                                                        <time><span class="pe-7s-timer"></span> {{gmdate("H:i:s",$course['duration'])}} </time>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @else
                                        <div class="row">
                                            <div class="col-12 col-md-6 offset-md-3">
                                                <div class="alert alert-warning fade show" role="alert">
                                                    آموزش مورد نظر پیدا نشد.
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                <div class="swiper-button-next">
                                    <i class="tio pe-7s-angle-left"></i>
                                </div>
                                <div class="swiper-button-prev">
                                    <i class="tio pe-7s-angle-right"></i>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-12 text-center mb-5">
                    <a href="{{route('tattoo.academy')}}" class="btn btn_md_primary rounded-pill border-1 c-dark text-center">
                        آموزش های تاتو
                    </a>
                    <a href="{{route('piercing.academy')}}" class="btn btn_md_primary rounded-pill border-1 c-dark text-center">
                        آموزش های پیرسینگ
                    </a>
                </div>

            </div>
        </section>--}}
        <!-- End featured courses -->

    </main>


@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.viewport.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/animsition.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/svg4everybody.legacy.min.js') }}"></script>
@endsection
