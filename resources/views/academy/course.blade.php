@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <!-- Start banner_about -->
        <section class="pt_banner_inner banner_px_image single_blog type_background">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-8 margin-t-10">
                        <div class="banner_title_inner">

                            <div class="about_post">
                                <span class="c_ategory">
                                    {{$body['course']['category']}}
                                </span>
                            </div>

                            <h1 class="margin-my-3 font-s-40 c-dark">
                                {{$body['course']['title']}}
                            </h1>
                            <p class="margin-my-3 font-s-1 c-dark">
                                {{$body['course']['subtitle']}}
                            </p>

                            <div class="footer_content">
                                <div class="item_auther">
                                    <div class="media">
                                        <img src="{{$body['course']['artist']['avatar']['thumbnail_file_name']}}" alt="">
                                        <div class="media-body my-auto">
                                            <div class="txt">
                                                <h4>{{$body['course']['artist']['name']}}</h4>
                                                <p>مدرس</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item_auther">
                                    <div class="media">
                                        <div class="media-body my-auto">
                                            <div class="txt">
                                                <h4>مدت زمان</h4>
                                                <p>{{$body['course']['duration']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="margin-t-5">
                                @if(auth()->check())
                                    @if($body['course']['wish'] == 0)
                                        <a href="{{route("{$body['prefix']}.academy.course.wish",['id' => $body['course']['id']])}}" class="btn btn_sm_primary p bg-green2 c-white rounded-pill">
                                            <i class="pe-7s-ribbon tio mr-1 align-middle font-s-20"></i>
                                            <span>نشان کن</span>
                                        </a>
                                    @else
                                        <a href="{{route("{$body['prefix']}.academy.course.wishNot",['id' => $body['course']['id']])}}" class="btn btn_sm_primary p bg-orange c-white rounded-pill">
                                            <i class="pe-7s-close-circle tio mr-1 align-middle font-s-20"></i>
                                            <span>حذف از لیست نشان</span>
                                        </a>
                                    @endif
                                @endif
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- End banner_about -->

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>


        <!-- Start content-Sblog -->
        <section class="content-Sblog" data-sticky-container>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-3 mb-5 mb-lg-0">
                        <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                            <div class="share_socail">

                                <button class="btn icon" data-toggle="tooltip" data-placement="right" title="فیسبوک"
                                        data-sharer="facebook" data-hashtag=""
                                        data-url="{{url()->current()}}">
                                    <i class="socicon-facebook tio"></i>
                                </button>

                                <button class="btn icon" data-toggle="tooltip" data-placement="right" title="توویتر"
                                        data-sharer="twitter" data-title=""
                                        data-url="https://twitter.com/intent/tweet?text={{url()->current()}}"
                                        data-to="">
                                    <i class="socicon-twitter tio"></i>
                                </button>

                                <button class="btn icon" data-toggle="tooltip" data-placement="right" title="تلگرام"
                                        data-sharer="telegram" data-title=""
                                        data-url="https://t.me/share/url?url={{rawurlencode(url()->current())}}&text={{rawurlencode($body['course']['title'])}}"
                                        data-to="">
                                    <i class="socicon-telegram tio"></i>
                                </button>


                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9">
                        <div class="body_content">

                            <div class="cover_video">
                                <img src="{{$body['course']['cover']['primary_file_name']}}" alt="">
                                <div class="icon_played">
                                    <button type="button" class="btn btn_video" data-toggle="modal"
                                            data-src="{{$body['course']['video_link']}}" data-target="#mdllVideo">
                                        <div class="scale rounded-circle b play_video">
                                            <i class="tio pe-7s-play"></i>
                                        </div>
                                    </button>
                                </div>
                            </div>

                            <div class="modal mdll_video fade" id="mdllVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                 aria-hidden="true">
                                <!-- Close -->
                                <button type="button" class="close bbt_close ripple_circle" data-dismiss="modal" aria-label="Close">
                                    <i class="tio pe-7s-close"></i>
                                </button>
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                                                        allow="autoplay"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {!! nl2br(e($body['course']['short_description'])) !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End. content-Sblog -->

        <!-- Start section_tag_auther -->
        <section class="section_tag_auther margin-b-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 offset-lg-3">
                        <div class="box_tags_psot">
                            <h4>برچسب ها</h4>
                            @foreach($body['course']['tags'] as $tag)
                                <a href="{{url('/academy?expr='.$tag)}}" class="btn">{{$tag}}</a>
                            @endforeach
                        </div>
                    </div>
                    {{--<div class="col-lg-9 offset-lg-3">
                        <div class="block_auther_post margin-t-5">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="media">
                                        <img class="img_person" src="{{asset('/img/academy/author.jpg')}}" alt="">
                                        <div class="media-body">
                                            <div class="txt">
                                                <h4>{{$body['artist']}}</h4>
                                                <p>من اسماعیل هستم و می خوام تجربه خودم رو با شما به اشتراک بگذارم.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </section>
        <!-- End. section_tag_auther -->

    </main>
    <!-- End main -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

