@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <!-- Start search bar -->
        <section class="margin-t-10">
            <div class="container-fluid">
                <div class="filter_form margin-t-1 aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                    <form class="row" method="GET" action="{{ route("{$body['prefix']}.academy") }}">
                        <div class="col-12 col-lg-6">
                            <div class="simple_search">
                                <div class="form-group">
                                    <div class="input_group">
                                        <input type="text" name="expr" class="form-control" placeholder="عبارت مورد نظر را جستجو کنید">
                                        <i class="pe-7s-search tio"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="form-group">
                                <select class="form-control custom-select" name="category">
                                    <option value="">دسته بندی</option>
                                    @if(!$body['categories']->isEmpty())
                                        @foreach($body['categories'] as $category)
                                            <option value="{{$category['id']}}">{{$category['name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <button type="submit" class="btn btn_md_primary btn-block scale border-0 sweep_letter sweep_top bg-blue c-white rounded-8">
                                <div class="inside_item">
                                    <span data-hover="جستجو کن">جستجو</span>
                                </div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- End search bar -->


        {{--<section class="academy-promo gng_serv_about padding-t-5" id="Works">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="item_ig item_mywork">
                            <div class="icon_played">
                                <button type="button" class="btn" data-toggle="modal" data-src="https://www.youtube.com/embed/VvHoHw5AWTk" data-target="#mdllVideo">
                                    <div class="scale rounded-circle play_video">
                                        <i class="tio pe-7s-play"></i>
                                    </div>
                                </button>
                            </div>
                            <a href="#" class="d-block">
                                <div class="mg_img">
                                    <img class="item_pic" src="{{asset('img/academy/blog-01.jpg')}}">
                                </div>
                                <div class="info_work">
                                    <h4>آموزش نحوه طراحی تصاویر اسلیمی</h4>
                                    <p>
                                        09 آبان 1399
                                    </p>
                                    <div class="link_view">مشاهده بیشتر</div>
                                </div>
                            </a>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item_ig item_mywork">
                            <div class="icon_played">
                                <button type="button" class="btn" data-toggle="modal" data-src="https://www.youtube.com/embed/VvHoHw5AWTk" data-target="#mdllVideo">
                                    <div class="scale rounded-circle play_video">
                                        <i class="tio pe-7s-play"></i>
                                    </div>
                                </button>
                            </div>
                            <a href="#" class="d-block">
                                <div class="mg_img">
                                    <img class="item_pic" src="{{asset('img/academy/blog-02.jpg')}}">
                                </div>
                                <div class="info_work">
                                    <h4>آموزش نحوه طراحی تصاویر سه بعدی</h4>
                                    <p>
                                        09 آبان 1399
                                    </p>
                                    <div class="link_view">مشاهده بیشتر</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </section>--}}


        <div class="container-fluid margin-my-7">
            <div class="dividar_line"></div>
        </div>

        <!-- Start Courses -->
        <section class="product_masonry">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 margin-b-12">

                        @if(!$body['courses']->isEmpty())
                        <div class="row">
                            @foreach($body['courses'] as $course)
                            <div class="col-12 col-lg-4">
                                <a href="{{url('/academy/course/'.$course['code'])}}" class="link_poet">
                                    <div class="grid_blog_avatar">
                                        <div class="cover_blog">
                                            <img src="{{$course['image']}}" alt="">
                                            <div class="icon_played">
                                                <a href="{{route("{$body['prefix']}.academy.course", ["code" => $course['code']])}}" class="btn btn_video">
                                                    <div class="scale rounded-circle b play_video">
                                                        <i class="tio pe-7s-link"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="body_blog">
                                            <a href="{{route("{$body['prefix']}.academy.course", ["code" => $course['code']])}}">
                                                <div class="person media">
                                                    <div class="media-body">
                                                        <div class="txt">
                                                            <h4>{{$course['category']}}</h4>
                                                            <h3>{{$course['title']}}</h3>
                                                            <time><span class="pe-7s-timer"></span>{{gmdate("H:i:s",$course['duration'])}}</time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach

                            @if($body['pagination']['last_page'] > 1)
                                <div class="col-12">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination default margin-t-5">

                                            @if($body['pagination']['current_page'] > 1)
                                                <li class="page-item">
                                                    <a class="page-link" href="{{url('/academy')}}" aria-label="Previous">
                                                        صفحه اول
                                                    </a>
                                                </li>
                                            @endif

                                            @if($body['pagination']['current_page'] > 1)
                                                <li class="page-item">
                                                    <a class="page-link" href="{{url('/academy?page='.($body['pagination']['current_page']-1))}}" aria-label="Previous">
                                                        <i class="pe-7s-angle-right tio"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            <li class="page-item"><a class="page-link active" href="{{url('/academy?page='.$body['pagination']['current_page'])}}">{{$body['pagination']['current_page']}}</a></li>
                                            @if($body['pagination']['current_page'] < $body['pagination']['last_page'])
                                                <li class="page-item">
                                                    <a class="page-link" href="{{url('/academy?page='.($body['pagination']['current_page']+1))}}" aria-label="Next">
                                                        <i class="pe-7s-angle-left tio"></i>
                                                    </a>
                                                </li>
                                            @endif

                                            @if($body['pagination']['current_page'] < $body['pagination']['last_page'])
                                                <li class="page-item">
                                                    <a class="page-link" href="{{url('/academy?page='.$body['pagination']['last_page'])}}" aria-label="Previous">
                                                        صفحه آخر {{'('.$body['pagination']['last_page'] .')'}}
                                                    </a>
                                                </li>
                                            @endif

                                        </ul>
                                    </nav>
                                </div>
                            @endif

                        </div>
                        @else
                            <div class="row">
                                <div class="col-12 col-lg-6 offset-lg-3">
                                    <div class="alert alert-warning fade show" role="alert">
                                        در حال حاضر آموزشی در این دسته بندی وجود ندارد.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- End Courses-->


    </main>
    <!-- End main -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

