

<!DOCTYPE html>
<html class="no-js" lang="en">

<head>

    <meta charset="utf-8">

    <!-- Page Title-->
    <title></title>

    <!-- Meta Tags-->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Viewport Meta-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Facebook Metadata Start -->
    <meta property="og:image:height" content="300">
    <meta property="og:image:width" content="573">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <!-- Facebook Metadata End -->

    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/img/layout/icon/fav/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/img/layout/icon/fav/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/img/layout/icon/fav/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/img/layout/icon/fav/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('/img/layout/icon/fav/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- Template Styles Start -->
    <link rel="stylesheet" type="text/css" href="{{asset('/css/index/plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/index/loader.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/index/main.css')}}">
    <!-- Template Styles End-->

</head>

<body class="main-fullscreen">

<!-- Main Screen Section Start -->
<section id="main" class="main-section fullscreen transparent">

    <!-- Fullscreen Background Start-->
        <div class="fullscreen-bg" style="background-image: url({{asset('/img/index/bg.jpg')}})">
            <!-- Dark gradient cover layer -->
            <div class="color-layer color-layer-medium"></div>
        </div>
    <!-- Fullscreen Background End-->

    <!-- Header Start -->
    <div class="main-section__header">
        <div class="logo">
            <!-- Your Logo Here -->
            <img src="{{asset('/img/index/logo.png')}}" alt="DigaranBodyArt">
        </div>
    </div>
    <!-- Header End -->

    <div class="container-fluid p-0 fullheight-mobile">
        <div class="row no-gutters flex-xl-row-reverse fullheight-mobile">
            <!-- Main Section Intro Start -->
            <div class="col-12 main-section__intro">
                <!-- Intro Content Start -->
                <div class="intro-content fullheight-mobile">
                    <!-- Headline Start -->
                    <div id="headline" class="headline center">
                        {{--<h1 class="large"><span class="outline-white">digaran body art</span></h1>--}}
                        <div class="headline__countdownholder">
                            <!-- Countdown Start-->
                            <div class="countdown countdown-headline links">
                                <ul>
                                    <li>
                                        <div>
                                            <a href="{{route('piercing')}}">
                                                <span class="thick">پـییرسـینگ</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <a href="{{route('tattoo')}}">
                                                <span class="thick">تـاتـو</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- Countdown End-->
                        </div>
                    </div>
                    <!-- Headline End -->
                </div>
                <!-- Intro Content End -->
            </div>
            <!-- Main Section Intro End -->
        </div>
    </div>

    <!-- Custom HTML End -->

</section>

<!-- Load Scripts Start-->
<script src="{{asset('/js/index/libs.min.js')}}"></script>
<script src="{{asset('/js/index/digaran-custom.js')}}"></script>
<!-- Load Scripts End-->

</body>

</html>
