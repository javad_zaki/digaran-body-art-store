<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>

@section('title')@show

    <!--=Meta Tags================================================================================================================-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="theme-color" content="#ffffff">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/img/layout/icon/fav/apple-touch-icon.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/img/layout/icon/fav/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/img/layout/icon/fav/favicon-16x16.png')}}">
        <link rel="manifest" href="{{asset('/img/layout/icon/fav/site.webmanifest')}}">
        <link rel="mask-icon" href="{{asset('/img/layout/icon/fav/safari-pinned-tab.svg')}}" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

    @section('meta')@show
    <!--=Meta Tags================================================================================================================-->


    <!--=Stylesheet================================================================================================================-->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-rtl.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/swiper.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/aos.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/font/Icon-font/pe-icon-7-stroke.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/font/socicon/socicon.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/font/Icon-font/helper.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/normalize.css') }}" type="text/css" />
    @section('stylesheet')@show

    <link rel="stylesheet" href="{{ asset('/css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/nav.css') }}" type="text/css" />
    <!--=Stylesheet================================================================================================================-->


</head>

<body>

<div id="wrapper">
    <div id="content">

        <input type="checkbox" id="ac-gn-menustate" class="ac-gn-menustate" />
        <nav id="ac-globalnav" class="no-js" role="navigation" aria-label="Global" data-hires="false" data-analytics-region="global nav" lang="fa" dir="rtl">
            <div class="ac-gn-content">
                <ul class="ac-gn-header">
                    <li class="ac-gn-item ac-gn-menuicon">
                        <label class="ac-gn-menuicon-label" for="ac-gn-menustate" aria-hidden="true">
                        <span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-top">
                            <span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-top"></span>
                        </span>
                        <span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-bottom">
						    <span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-bottom"></span>
					    </span>
                        </label>
                        <a href="#ac-gn-menustate" role="button" class="ac-gn-menuanchor ac-gn-menuanchor-open" id="ac-gn-menuanchor-open">
                            <span class="ac-gn-menuanchor-label">باز کردن منوی اصلی</span>
                        </a>
                        <a href="#" role="button" class="ac-gn-menuanchor ac-gn-menuanchor-close" id="ac-gn-menuanchor-close">
                            <span class="ac-gn-menuanchor-label">بستن منوی اصلی</span>
                        </a>
                    </li>
                    <li class="ac-gn-item ac-gn-dba">
                        <a class="ac-gn-link ac-gn-link-dba" href="{{route('index')}}" id="ac-gn-firstfocus-small">
                            <span class="ac-gn-link-text">دیگران</span>
                        </a>
                    </li>
                    <li class="ac-gn-item ac-gn-bag ac-gn-bag-small" id="ac-gn-bag-small">
                        <div class="ac-gn-bag-wrapper">
                            <a class="ac-gn-link ac-gn-link-bag" href="{{route('shopping.bag')}}" aria-label="Shopping Bag">
                                <span class="ac-gn-link-text">Shopping Bag</span>
                                <span class="ac-gn-bag-badge" style="{{(session()->has('bag.items')) ? (count(session()->get('bag.items')) > 0) ? 'display:inline-block;' : '' : ''}}">
                                <span class="ac-gn-bag-badge-separator"></span>
                                <span class="ac-gn-bag-badge-number">{{(session()->has('bag.items')) ? (count(session()->get('bag.items')) > 0) ? count(session()->get('bag.items')) : '' : ''}}</span>
                                <span class="ac-gn-bag-badge-unit">+</span>
					        </span>
                            </a>
                        </div>
                    </li>
                </ul>
                <div class="ac-gn-search-placeholder-container" role="search">
                    <div class="ac-gn-search ac-gn-search-small">
                        <a id="ac-gn-link-search-small" class="ac-gn-link" href="{{ route('search') }}">
                            <div class="ac-gn-search-placeholder-bar">
                                <div class="ac-gn-search-placeholder-input">
                                    <div class="ac-gn-search-placeholder-input-text" aria-hidden="true">
                                        <div class="ac-gn-link-search ac-gn-search-placeholder-input-icon"></div>
                                        <span class="ac-gn-search-placeholder">جستجو در ...</span>
                                    </div>
                                </div>
                                <div class="ac-gn-searchview-close ac-gn-searchview-close-small ac-gn-search-placeholder-searchview-close">
                                    <span class="ac-gn-searchview-close-cancel" aria-hidden="true">انصراف</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <ul class="ac-gn-list">
                    <li class="ac-gn-item ac-gn-dba">
                        <a class="ac-gn-link ac-gn-link-dba" href="{{route('index')}}" id="ac-gn-firstfocus">
                            <span class="ac-gn-link-text">دیگران</span>
                        </a>
                    </li>
                    @if (\Illuminate\Support\Facades\Request::is('tattoo') || \Illuminate\Support\Facades\Request::is('tattoo/*') || session()->get('prefix') == 'tattoo')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                            <a class="ac-gn-link ac-gn-link-iphone" href="{{route('tattoo')}}">
                                <span class="ac-gn-link-text">ابتدا</span>ابتدا
                            </a>
                        </li>

                    @elseif(\Illuminate\Support\Facades\Request::is('piercing') || \Illuminate\Support\Facades\Request::is('piercing/*') || session()->get('prefix') == 'piercing')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                            <a class="ac-gn-link ac-gn-link-iphone" href="{{route('piercing')}}">
                                <span class="ac-gn-link-text">ابتدا</span>ابتدا
                            </a>
                        </li>

                    @else

                       {{-- <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                            <a class="ac-gn-link ac-gn-link-iphone" href="{{route('index')}}">
                                <span class="ac-gn-link-text">ابتدا</span>ابتدا
                            </a>
                        </li>--}}

                    @endif


                    @if (\Illuminate\Support\Facades\Request::is('tattoo') || \Illuminate\Support\Facades\Request::is('tattoo/*') || session()->get('prefix') == 'tattoo')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone" id="ac-menuanchor-products">
                            <a class="ac-gn-link ac-gn-link-iphone" href="#" role="button" onclick="submenu()">
                                <span class="ac-gn-link-text">محصولات</span>محصولات
                            </a>
                        </li>

                    @elseif(\Illuminate\Support\Facades\Request::is('piercing') || \Illuminate\Support\Facades\Request::is('piercing/*') || session()->get('prefix') == 'piercing')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone" id="ac-menuanchor-products">
                            <a class="ac-gn-link ac-gn-link-iphone" href="#" role="button" onclick="submenu()">
                                <span class="ac-gn-link-text">محصولات</span>محصولات
                            </a>
                        </li>

                    @endif

                    {{--@if (\Illuminate\Support\Facades\Request::is('tattoo') || \Illuminate\Support\Facades\Request::is('tattoo/*') || session()->get('prefix') == 'tattoo')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                            <a class="ac-gn-link ac-gn-link-iphone" href="{{route('tattoo.academy')}}">
                                <span class="ac-gn-link-text">آکادمی</span>آکادمی
                            </a>
                        </li>

                    @elseif(\Illuminate\Support\Facades\Request::is('piercing') || \Illuminate\Support\Facades\Request::is('piercing/*') || session()->get('prefix') == 'piercing')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                            <a class="ac-gn-link ac-gn-link-iphone" href="{{route('piercing.academy')}}">
                                <span class="ac-gn-link-text">آکادمی</span>آکادمی
                            </a>
                        </li>

                    @endif--}}

                    @if (\Illuminate\Support\Facades\Request::is('tattoo') || \Illuminate\Support\Facades\Request::is('tattoo/*') || session()->get('prefix') == 'tattoo')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                            <a class="ac-gn-link ac-gn-link-iphone" href="{{route('piercing')}}">
                                <span class="ac-gn-link-text">پییرسینگ</span>پییرسینگ
                            </a>
                        </li>

                    @elseif(\Illuminate\Support\Facades\Request::is('piercing') || \Illuminate\Support\Facades\Request::is('piercing/*') || session()->get('prefix') == 'piercing')

                        <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                            <a class="ac-gn-link ac-gn-link-iphone" href="{{route('tattoo')}}">
                                <span class="ac-gn-link-text">تاتو</span>تاتو
                            </a>
                        </li>

                    @else

                    @endif

                    <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone d-md-none">
                        <a class="ac-gn-link ac-gn-link-iphone" href="{{route('user')}}">
                            <span class="ac-gn-link-text">حساب کاربری</span>حساب کاربری
                        </a>
                    </li>

                    <li class="ac-gn-item ac-gn-item-menu ac-gn-search" role="search">
                        <a id="ac-gn-link-search" class="ac-gn-link ac-gn-link-search" href="{{route('search')}}" data-analytics-title="search" data-analytics-click="search" data-analytics-intrapage-link aria-label="جستجو در ..."></a>
                    </li>
                    <li class="ac-gn-item ac-gn-item-menu ac-gn-search">
                        <a id="ac-gn-link-user" class="ac-gn-link ac-gn-link-user" href="{{route('user')}}"></a>
                    </li>
                    <li class="ac-gn-item ac-gn-bag" id="ac-gn-bag">
                        <div class="ac-gn-bag-wrapper">
                            <a class="ac-gn-link ac-gn-link-bag" href="{{route('shopping.bag')}}" data-analytics-title="bag" data-analytics-click="bag" aria-label="Shopping Bag" data-string-badge="Shopping Bag with item count : {%BAGITEMCOUNT%}">
                                <span class="ac-gn-link-text">Shopping Bag</span>
                                <span class="ac-gn-bag-badge" style="{{(session()->has('bag.items')) ? (count(session()->get('bag.items')) > 0) ? 'display:inline-block;' : '' : ''}}">
                                <span class="ac-gn-bag-badge-separator"></span>
                                <span class="ac-gn-bag-badge-number">{{(session()->has('bag.items')) ? (count(session()->get('bag.items')) > 0) ? count(session()->get('bag.items')) : '' : ''}}</span>
                                <span class="ac-gn-bag-badge-unit">+</span>
					        </span>
                            </a>
                        </div>
                        <span class="ac-gn-bagview-caret ac-gn-bagview-caret-large"></span>
                    </li>
                </ul>
                <aside id="ac-gn-searchview" class="ac-gn-searchview" role="search" data-analytics-region="search">
                    <div class="ac-gn-searchview-content">
                        <div class="ac-gn-searchview-bar">
                            <div class="ac-gn-searchview-bar-wrapper">
                                <form id="ac-gn-searchform" class="ac-gn-searchform" action="{{route('search')}}" method="get">
                                    <div class="ac-gn-searchform-wrapper">
                                        <input id="ac-gn-searchform-input" name="expr" class="ac-gn-searchform-input" type="text" aria-label="جستجو در" placeholder="جستجو در ..." autocapitalize="off" autocomplete="off" />
                                        <button id="ac-gn-searchform-submit" class="ac-gn-searchform-submit" type="submit" disabled aria-label="Submit Search"> </button>
                                        <button id="ac-gn-searchform-reset" class="ac-gn-searchform-reset" type="reset" disabled aria-label="Clear Search">
                                            <span class="ac-gn-searchform-reset-background"></span>
                                        </button>
                                    </div>
                                </form>
                                <button id="ac-gn-searchview-close-small" class="ac-gn-searchview-close ac-gn-searchview-close-small" aria-label="انصراف از جستجو">
                                    <span class="ac-gn-searchview-close-cancel" aria-hidden="true">
                                        انصراف
                                    </span>
                                </button>
                            </div>
                        </div>
                        <aside id="ac-gn-searchresults" class="ac-gn-searchresults" data-string-quicklinks="Quick Links" data-string-suggestions="Suggested Searches" data-string-noresults=""></aside>
                    </div>
                    <button id="ac-gn-searchview-close" class="ac-gn-searchview-close" aria-label="Cancel Search">
                        <span class="ac-gn-searchview-close-wrapper">
                            <span class="ac-gn-searchview-close-left"></span>
                            <span class="ac-gn-searchview-close-right"></span>
                        </span>
                    </button>
                </aside>
            </div>
        </nav>
        <div class="ac-gn-blur"></div>
        <div id="ac-gn-curtain" class="ac-gn-curtain"></div>
        <div id="ac-gn-placeholder" class="ac-nav-placeholder"></div>

        <nav id="chapternav" class="chapternav">
            <div class="chapternav-wrapper">
                <ul class="chapternav-items">
                    @foreach(session()->get('categories') as $category)
                        <li class="chapternav-item">
                            <a class="chapternav-link" href="{{url(session()->get('prefix').'/store?category='.$category['id'])}}">
                                <figure class="chapternav-icon" {{--style="background-image: url({{$category['image']}})"--}}></figure>
                                <span class="chapternav-label" role="text">{{$category['name']}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="chapternav-paddles">
                    <button class="chapternav-paddle chapternav-paddle-left" aria-hidden="true" disabled></button>
                    <button class="chapternav-paddle chapternav-paddle-right" aria-hidden="true" disabled></button>
                </div>
            </div>
        </nav>

    @section('category')@show

    @section('body')@show

    <!-- [id] content -->
        <footer class="footer_short bg-white padding-t-12">
            <div class="container">
                <div class="row justify-content-md-center text-center">
                    <div class="col-md-8">
                        <a class="logo c-dark" href="">
                            <img src="{{asset('/img/layout/footer-logo.png')}}" alt="">
                        </a>
                        <div class="social--media">
                            <a href="#" class="btn so-link">
                                <div class="instagram">
                                    <i class="socicon-instagram"></i>
                                </div>
                            </a>
                            <a href="#" class="btn so-link">
                                <div class="youtube">
                                    <i class="socicon-youtube"></i>
                                </div>
                            </a>
                            <a href="#" class="btn so-link">
                                <div class="facebook">
                                    <i class="socicon-facebook"></i>
                                </div>
                            </a>
                            <a href="#" class="btn so-link">
                                <div class="pinterest">
                                    <i class="socicon-pinterest"></i>
                                </div>
                            </a>
                        </div>
                        <div class="other--links">
                            {{--@if (\Illuminate\Support\Facades\Request::is('tattoo') || \Illuminate\Support\Facades\Request::is('tattoo/*') || session()->get('prefix') == 'tattoo')
                                <a href="{{route('piercing')}}">پییرسینگ</a>
                            @elseif(\Illuminate\Support\Facades\Request::is('piercing') || \Illuminate\Support\Facades\Request::is('piercing/*') || session()->get('prefix') == 'piercing')
                                <a href="{{route('tattoo')}}">تاتو</a>
                            @else

                            @endif--}}
                            <a href="{{route('user')}}">حساب کاربری</a>
                            <a href="{{route('support')}}">تماس</a>
                            <a href="{{route('about')}}">درباره</a>
                        </div>
                        <div class="opyright">
                            <p>
                                کلیه حقوق این وب سایت متعلق به برند <a href="{{route('about')}}" target="_blank">دیگران</a>  است.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        {{--<div class="prgoress_indicator active-progress">
            <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
                <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919, 307.919; stroke-dashoffset: 18.4471;"></path>
            </svg>
        </div>--}}

       {{-- <section class="loading_overlay">
            <div class="loader_logo">
                <img class="logo" src="{{asset('/img/layout/blacklogo.png')}}">
            </div>
        </section>--}}

    </div>
</div>

    <!--=Script================================================================================================================-->
    @section('head-script')@show
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/particles.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/TweenMax.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/ScrollMagic.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/animation.gsap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/debug.addIndicators.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/swiper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/countdown.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/simpleParallax.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/charming.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/imagesloaded.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.bxslider.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/sharer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/sticky.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/aos.js') }}"></script>
    @section('tail-script')@show
    <script type="text/javascript" src="{{ asset('/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/nav.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/nav-paddle-rtl.js') }}"></script>
    <script>
        function submenu(){
            var x = document.getElementById("chapternav");
            if (x.classList.contains("hidden")) {
                x.classList.add("visible");
                x.classList.remove("hidden");
            } else if(x.classList.contains("visible")) {
                x.classList.add("hidden");
                x.classList.remove("visible");
            } else{
                x.classList.add("visible");
            }
        }
    </script>
    <!--=Script================================================================================================================-->
</body>
</html>
