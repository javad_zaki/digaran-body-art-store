<!DOCTYPE html>
<html lang="fa">
<head>

@section('title')@show

    <!--=Meta Tags================================================================================================================-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="theme-color" content="#ffffff">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('meta')@show
    <!--=Meta Tags================================================================================================================-->


    <!--=Stylesheet================================================================================================================-->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-rtl.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/swiper.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/aos.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/font/Icon-font/pe-icon-7-stroke.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/font/socicon/socicon.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/font/icon-font/helper.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/normalize.css') }}" type="text/css" />
    @section('stylesheet')@show

    <link rel="stylesheet" href="{{ asset('/css/style.css') }}" type="text/css" />
    <!--=Stylesheet================================================================================================================-->


</head>

<body >

<div id="wrapper">
    <div id="content">

        @section('body')@show

    </div>
</div>

<!--=Script================================================================================================================-->
@section('head-script')@show
<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/particles.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/animation.gsap.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/debug.addIndicators.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/swiper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/countdown.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/simpleParallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/charming.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/imagesloaded.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/sharer.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/sticky.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/aos.js') }}"></script>
@section('tail-script')@show
<script type="text/javascript" src="{{ asset('/js/script.js') }}"></script>

<!--=Script================================================================================================================-->
</body>
</html>

