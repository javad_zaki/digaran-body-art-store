@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <section class="margin-t-10 margin-b-3">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-12">
                        <div class="container">
                            <div class="banner_title">
                                <h1>
                                    پرداخت صورت حساب
                                </h1>
                                <p>
                                    پرداخت امن و آسان
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="shop_bag_total margin-b-10">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <div class="detail">
                            <div class="row">
                                <div class="col-6">
                                    <h4 class="subtotal_text">تعداد کل اقلام سفارش</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="subtotal_amount">{{(session()->has('bag.invoice')) ? session('bag.invoice')['qty'] : 0}} عدد </h4>
                                </div>
                            </div>
                            <div class="row margin-t-2">
                                <div class="col-6">
                                    <h4 class="subtotal_text">جمع کل</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="subtotal_amount">{{(session()->has('bag.invoice')) ? number_format(session('bag.invoice')['subtotal']) : 0}} ریال </h4>
                                </div>
                            </div>
                            <div class="row margin-t-2">
                                <div class="col-6">
                                    <h4 class="subtotal_text">مالیات بر ارزش افزوده</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="subtotal_amount">{{(session()->has('bag.invoice')) ? number_format(session('bag.invoice')['tax']) : 0}} ریال </h4>
                                </div>
                            </div>
                            <div class="row margin-t-2">
                                <div class="col-6">
                                    <h4 class="subtotal_text">حمل و نقل</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="subtotal_amount">{{(session()->has('bag.invoice')) ? number_format(session('bag.invoice')['shipping']) : 0}} ریال </h4>
                                </div>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="total">
                            <div class="row margin-t-2">
                                <div class="col-6">
                                    <h4 class="total_text">مجموع</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="total_amount">{{(session()->has('bag.invoice')) ? number_format(session('bag.invoice')['total']) : 0}} ریال </h4>
                                </div>
                            </div>
                        </div>
                        <div class="checkout">
                            <a href="{{route("shopping.payment")}}" class="btn btn_md_primary btn-block bg-blue rounded-8 c-white h-fit-content">
                                پرداخت
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <!-- End main -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

