@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <section class="margin-t-10 margin-b-3">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-12">
                        <div class="container">
                            <div class="banner_title">
                                <h1>
                                    آدرس و اطلاعات گیرنده
                                </h1>

                                @if(auth()->check())
                                    <p>
                                        اطلاعات گیرنده را به دقت وارد نمایید.
                                    </p>
                                @else
                                    <p>
                                        سفارش شما به عنوان<span class="c-purple">مهمان</span> ثبت خواهد شد. آیا قبلا حساب کاربری ساخته ایید؟ <a href="{{url('user/profile')}}"> ورود به حساب کاربری </a>
                                    </p>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="checkout_address margin-t-1 margin-b-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <div class="card justify-content-center">
                            <div class="card-body">
                                <form method="post" action="{{route('shopping.recipient')}}" class="row" id="recipient_form">
                                    @csrf

                                    @if(auth()->check())
                                        {{--<div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <label>برای چه کسی است؟</label>
                                                <select class="form-control custom-select" name="recipient" id="recipient" onchange="who(this)">
                                                    <option value="me" selected>خودم</option>
                                                    <option value="other">دیگری</option>
                                                </select>
                                                {!! $errors->first('recipient', '<span class="text-warning font-small-2">:message</span>') !!}
                                            </div>
                                        </div>--}}
                                        <input type="hidden" name="recipient" id="recipient" value="me">
                                    @else
                                        <input type="hidden" name="recipient" id="recipient" value="guest">
                                    @endif
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>نام و نام خانوادگی</label>
                                            <input type="text" class="form-control" placeholder="نام و نام خانوادگی خود را وارد کنید" id="name" name="name" value="">
                                            {!! $errors->first('name', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>شماره ملی</label>
                                            <input type="text" class="form-control" placeholder="شماره ملی خود را وارد کنید" id="national_id" name="national_id" value="">
                                            {!! $errors->first('national_id', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>شماره موبایل</label>
                                            @if(auth()->check())
                                                <input type="text" class="form-control" placeholder="شماره موبایل خود را وارد کنید" id="mobile" name="mobile" value="" readonly>
                                            @else
                                                <input type="text" class="form-control" placeholder="شماره موبایل خود را وارد کنید" id="mobile" name="mobile" value="">
                                            @endif
                                            {!! $errors->first('mobile', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>کد پستی</label>
                                            <input type="text" class="form-control" placeholder="شماره موبایل خود را وارد کنید" id="postal_code" name="postal_code" value="">
                                            {!! $errors->first('postal_code', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>استان</label>
                                            <input type="text" class="form-control" placeholder="استان را وارد نمایید" id="province" name="province" value="">
                                            {!! $errors->first('province', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>شهر</label>
                                            <input type="text" class="form-control" placeholder="شهر را وارد نمایید" id="city" name="city" value="">
                                            {!! $errors->first('city', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-12">
                                        <div class="form-group">
                                            <label>آدرس کامل</label>
                                            <input type="text" class="form-control" placeholder="آدرس دقیق خود را وارد کنید" id="address" name="address" value="">
                                            {!! $errors->first('address', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>توضیحات</label>
                                            <textarea class="form-control" rows="2" placeholder="اگر نکته ایی مد نظر دارید، برای ما یادداشت بگذارید" id="description" name="description"></textarea>
                                            {!! $errors->first('description', '<span class="text-warning font-small-2">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn_md_primary btn-block bg-blue rounded-8 c-white h-fit-content">
                                            ادامه
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <!-- End main -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>


    <script>

        $(document).ready(function() {
            initialize();
        });

        function who(obj) {
            if(obj.value === 'other'){
                document.getElementById('recipient_form').reset();
                document.getElementById('mobile').readOnly = false;
                obj.value = 'other';
            }else if(obj.value === 'me'){
                initialize();
                document.getElementById('mobile').readOnly = true;
                obj.value = 'me';
            }else{
                initialize();
            }
        }

        function initialize() {
            $('#name').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['name'] : (isset(auth()->user()['name']) ? auth()->user()['name'] : '')}}');
            $('#national_id').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['national_id'] : (isset(auth()->user()['national_id']) ? auth()->user()['national_id'] : '')}}');
            $('#mobile').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['mobile'] : (isset(auth()->user()['mobile']) ? auth()->user()['mobile'] : '')}}');
            $('#postal_code').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['postal_code'] : (isset(auth()->user()['postal_code']) ? auth()->user()['postal_code'] : '')}}');
            $('#province').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['province'] : (isset(auth()->user()['province']) ? auth()->user()['province'] : '')}}');
            $('#city').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['city'] : (isset(auth()->user()['city']) ? auth()->user()['city'] : '')}}');
            $('#address').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['address'] : (isset(auth()->user()['address']) ? auth()->user()['address'] : '')}}');
            $('#description').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['description'] : ''}}');
            $('#recipient').val('{{(session()->has('bag.shipping')) ? session()->get('bag.shipping')['recipient'] : 'me'}}');
        }
    </script>
@endsection

