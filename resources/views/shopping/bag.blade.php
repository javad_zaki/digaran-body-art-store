@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <section class="margin-t-10 margin-b-6">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-12">
                        <div class="container">
                            <div class="banner_title">
                                <h1>
                                    بررسی سبد خرید
                                </h1>
                                <p>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <form method="post" action="{{route('shopping.confirm')}}">
            @csrf
        <section class="product_review_bag margin-t-5 margin-b-5">
            <div class="container">

                @if(session()->has('message'))
                    <div class="row">
                        <div class="col-12 col-lg-6 offset-lg-3">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert"> {{session()->get('message')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="pe-7s-close tio"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                @endif

                @if(!empty(session()->get('bag.items')))
                    @foreach(session('bag.items') as $item)
                        <div class="bag_item">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-4">
                                    <div class="mockup">
                                        <img class="slide-in-bottom" src="{{$item['image']}}" alt="">
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 col-lg-8">
                                    <div class="detail">
                                        <div class="product_name">
                                            <h1>{{$item['name']}}</h1>
                                        </div>
                                        {{--<div class="product_price margin-t-2">
                                            <h2><span class="numeral">{{number_format($item['base_price'])}}</span> ریال </h2>
                                        </div>--}}
                                        <div class="discount margin-t-2">
                                            <div class="row">
                                                <div class="col-6">
                                                    <p class="title">قیمت محصول</p>
                                                </div>
                                                <div class="col-6">
                                                    <p class="final_price"><span class="numeral">{{number_format($item['base_price'])}}</span> ریال </p>
                                                </div>
                                            </div>
                                            @if($item['discount'] > 0)
                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="title">درصد تخفیف</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <p class="amount">%<span class="numeral">{{$item['discount']}}</span></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <p class="title">قیمت پس از تخفیف</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <p class="final_price"><span class="numeral">{{number_format($item['price_after_discount'])}}</span> ریال </p>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="row">
                                                <div class="col-6">
                                                    <p class="title c-green">مجموع</p>
                                                </div>
                                                <div class="col-6">
                                                    <p class="final_price c-green"><span class="total_price_amount numeral" id="{{$item['sku'].'_total'}}" data-total="{{$item['total']}}">{{number_format($item['total'])}}</span> ریال </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-my-1">
                                            <div class="dividar_line"></div>
                                        </div>
                                        <div class="actions margin-t-2">
                                            @if($item['unavailable'])
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="alert alert-warning alert-dismissible fade show" role="alert"> متاسفانه موجودی کافی نیست. می توانید تا سقف <span class="numeral">{{$item['stoke']}}</span> عدد از این محصول سفارش دهید.
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <i class="pe-7s-close tio"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="row">
                                                {{--<div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                                    <div class="form-group">
                                                        <select class="form-control custom-select mt-lg-2">
                                                            <option>رنگ محصول</option>
                                                            @foreach($item['colors'] as $color)
                                                                <option value="{{$color}}">{{$color}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>--}}
                                                <div class="col-6 col-md-4 col-lg-2">
                                                    <div class="quantity">
                                                        <input type="number" name="{{$item['sku']}}" id="{{$item['sku']}}" min="1" max="500" value="{{$item['qty']}}" data-price="{{$item['price_after_discount']}}"  onchange="qty(this)" placeholder="تعداد">
                                                       {{-- <select class="form-control custom-select" name="{{$item['sku']}}" id="{{$item['sku']}}" data-price="{{$item['price_after_discount']}}" onchange="qty(this)">
                                                            @for($i=1; $i<=10; $i++)
                                                                <option value="{{$i}}" {{ ($item['qty'] == $i)? "selected" : "" }}>{{$i}}</option>
                                                            @endfor
                                                        </select>--}}
                                                    </div>
                                                </div>
                                                <div class="col-6 col-md-4 col-lg-2">
                                                    <a href="{{route('shopping.bag.remove', ['sku' => $item['sku']])}}" class="btn btn-block btn-lg border-1-red c-red rounded-8 mt-2">
                                                        <span>حذف</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

            </div>
        </section>

        <div class="container margin-my-1">
            <div class="dividar_line"></div>
        </div>

        <section class="shop_bag_total margin-b-10">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-12 col-md-8">
                        <div class="detail">
                            <div class="row">
                                <div class="col-6">
                                    <h4 class="subtotal_text">جمع کل</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="subtotal_amount"><span class="numeral" id="subtotal" data-subtotal="0">0</span> ریال </h4>
                                </div>
                            </div>
                            <div class="row margin-t-2">
                                <div class="col-6">
                                    <h4 class="subtotal_text">حمل و نقل</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="subtotal_amount"><span class="numeral" id="shipping" data-shipping="0">0</span> ریال </h4>
                                </div>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="total">
                            <div class="row margin-t-2">
                                <div class="col-6">
                                    <h4 class="total_text">مجموع</h4>
                                </div>
                                <div class="col-6">
                                    <h4 class="total_amount"><span class="numeral" id="total" data-total="0">0</span> ریال </h4>
                                </div>
                            </div>
                        </div>
                        <div class="checkout">
                            <button type="submit" class="btn btn_md_primary btn-block bg-blue rounded-8 c-white h-fit-content">
                                ادامه
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </form>

        @else
            <div class="row">
                <div class="col-12 col-lg-6 offset-lg-3">
                    <div class="alert alert-warning fade show" role="alert">
                        محصولی در سبد خرید شما وجود ندارد.
                    </div>
                </div>
            </div>
        @endif

    </main>
    <!-- End main -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>

    <script>

        $(document).ready(function() {
            invoice();
        });

        function qty(obj) {
            let total = obj.dataset.price * obj.value;
            $("#"+obj.id+"_total").html(new Intl.NumberFormat().format(total));
            let el = document.getElementById(obj.id+"_total");
            el.dataset.total = total.toString();

            invoice();
        }

        function invoice() {
            let subtotal = 0;
            let shipping = document.getElementById("shipping");
            let items = document.querySelectorAll(".total_price_amount");

            items.forEach(function (item) {
                subtotal += parseInt(item.dataset.total);
            });

            $("#subtotal").html(new Intl.NumberFormat().format(subtotal));
            $("#total").html(new Intl.NumberFormat().format(subtotal + parseInt(shipping.dataset.shipping)));
        }

        jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><i class="icon pe-7s-angle-up"></i></div><div class="quantity-button quantity-down"><i class="icon pe-7s-angle-down"></i></div></div>').insertBefore('.quantity input');
        jQuery('.quantity').each(function() {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.quantity-up'),
                btnDown = spinner.find('.quantity-down'),
                min = input.attr('min'),
                max = input.attr('max');

            btnUp.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            btnDown.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

        });

    </script>
@endsection

