@extends('layouts.guest')

@section('title')
    <title></title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
@endsection


@section('body')

    <!-- Stat main -->
    <main>

        <!-- Start Banner Section -->
        <section class="demo_1 banner_section user_profile margin-b-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-6 my-auto">
                        <div class="banner_title">
                            <h1>حساب کاربری</h1>
                            <p>
                                مدیریت حساب کاربری
                            </p>
                            <a href="{{route('user.password')}}" class="btn btn_md_primary rounded-8 bg-blue c-white">تغییر رمز عبور</a>
                            <a href="{{ route('logout') }}" class="btn btn_md_primary rounded-7 border-1 text-center"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">خروج از حساب</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 mt-md-0 mt-lg-5">
                        <div class="block__srarch">
                            <div class="title__search">
                                <h2>اطلاعات و نشانی</h2>
                                <p>این اطلاعات برای ارسال سفارش ها ضروری است.</p>
                            </div>
                            <form action="{{ route('user.update') }}" method="POST" class="row">
                                @csrf
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>نام و نام خانوادگی</label>
                                        <input type="text" class="form-control" placeholder="نام و نام خانوادگی خود را وارد کنید" name="name" id="name" value="{{$body['user']['name']}}">
                                        {!! $errors->first('name', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>شماره ملی</label>
                                        <input type="text" class="form-control" placeholder="شماره ملی خود را وارد کنید" name="national_id" id="national_id" value="{{$body['user']['national_id']}}">
                                        {!! $errors->first('national_id', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>شماره موبایل</label>
                                        <input type="text" class="form-control" readonly value="{{$body['user']['mobile']}}">
                                        {!! $errors->first('mobile', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>ایمیل</label>
                                        <input type="text" class="form-control" placeholder="ایمیل برای ارسال فاکتور الکترونیک لازم است" name="email" id="email" value="{{$body['user']['email']}}">
                                        {!! $errors->first('email', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>کد پستی</label>
                                        <input type="text" class="form-control" placeholder="کد پستی خود را وارد کنید" name="postal_code" id="postal_code" value="{{$body['user']['postal_code']}}">
                                        {!! $errors->first('postal_code', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>استان</label>
                                        <input type="text" class="form-control" placeholder="استان را وارد کنید" name="province" id="province" value="{{$body['user']['province']}}">
                                        {!! $errors->first('province', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>شهر</label>
                                        <input type="text" class="form-control" placeholder="شهر را وارد کنید" name="city" id="city" value="{{$body['user']['city']}}">
                                        {!! $errors->first('city', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label>آدرس کامل</label>
                                        <input type="text" class="form-control" placeholder="لطفا آدرس دقیق را وارد کنید" name="address" id="address" value="{{$body['user']['address']}}">
                                        {!! $errors->first('address', '<span class="text-warning font-small-2">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn__store">ذخیره</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner -->

        <section class="demo_1 banner_section user_profile margin-b-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- Start tb_features_pricing -->
                        <section class="tb_features_pricing padding-t-10">
                            <div class="container">
                                <div class="row justify-content-center text-center">
                                    <div class="col-lg-5 margin-b-2">
                                        <div class="title_sections_inner">
                                            <h2>سوابق خرید</h2>
                                            <p>پیگیری سفارش ها و زمان ارسال آنها</p>
                                        </div>
                                    </div>
                                </div>
                                @if(!$body['orders']->isEmpty())
                                <div class="row">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col"><span>#شماره پیگیری</span></th>
                                                    <th scope="col"><span>تعداد اقلام</span></th>
                                                    <th scope="col"><span>مجموع اقلام (ریال)</span></th>
                                                    <th scope="col"><span>حمل و نقل (ریال)</span></th>
                                                    <th scope="col"><span>مالیات (ریال)</span></th>
                                                    <th scope="col"><span>مجموع سفارش (ریال)</span></th>
                                                    <th scope="col"><span>مبلغ قابل پرداخت (ریال)</span></th>
                                                    <th scope="col"><span>صورت حساب کاغذی</span></th>
                                                    <th scope="col"><span>آخرین به روز رسانی</span></th>
                                                    <th scope="col"><span>وضعیت</span></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($body['orders'] as $item)
                                                        <tr>
                                                            <td><span>#{{$item['code']}}</span></td>
                                                            <td><span>{{$item['qty']}}</span></td>
                                                            <td><span>{{number_format($item['subtotal'])}}</span></td>
                                                            <td><span>{{number_format($item['shipping'])}}</span></td>
                                                            <td><span>{{number_format($item['tax'])}}</span></td>
                                                            <td><span>{{number_format($item['total'])}}</span></td>
                                                            <td><span>{{number_format($item['payable'])}}</span></td>
                                                            <td><span>{{($item['send_paper_invoice']) ? 'می خواهم' : 'نمی خواهم'}}</span></td>
                                                            <td><span>{{$item['updated_at']}}</span></td>
                                                            <td>
                                                                @switch($item['status'])
                                                                    @case('pending')
                                                                    <span class="c-magenta">در حال بررسی</span>
                                                                    @break
                                                                    @case('queued')
                                                                    <span class="c-blue">در صف ارسال</span>
                                                                    @break
                                                                    @case('sent')
                                                                    <span class="c-green">ارسال شد</span>
                                                                    @break
                                                                    @case('unpaid')
                                                                    <span class="c-orange">عدم پرداخت</span>
                                                                    @break
                                                                @endswitch
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="row">
                                    <div class="col-12 col-lg-8 offset-lg-2">
                                        <div class="alert alert-warning fade show" role="alert">
                                             در حال حاضر سفارشی برای شما ثبت نشده است.
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </section>
                        <!-- End. tb_features_pricing -->

                    </div>
                </div>
            </div>
        </section>

        <!-- Start featured products -->
        {{--<section class="section__stories feature_strories user_profile_wish margin-b-7">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="swip__stories">
                            <div class="swiper-container feature_strories">
                                <div class="title_sections">
                                    <h4>محصول های نشان شده</h4>
                                </div>
                                @if(!$body['products']->isEmpty())
                                <div class="swiper-wrapper">
                                    @foreach($body['products'] as $product)
                                        <div class="swiper-slide">
                                            <a href="{{route("{$product['domain']}.store.product", ["sku" => $product['sku']])}}" class="item">
                                                <div class="img__nature">
                                                    <img src="{{$product['image']}}">
                                                </div>
                                                <div class="inf__txt">
                                                    <h3>{{$product['name']}}</h3>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="swiper-button-next">
                                    <i class="tio pe-7s-angle-right"></i>
                                </div>
                                <div class="swiper-button-prev">
                                    <i class="tio pe-7s-angle-left"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                    <div class="row">
                        <div class="col-12 col-lg-6 offset-lg-3">
                            <div class="alert alert-warning fade show" role="alert">
                                 هنوز محصولی را به لیست علاقه مندی های خود اضافه نکرده ایید.
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </section>--}}
        <!-- End. featured products -->


        <!-- Start featured courses -->
        {{--<section class="section__stories blog_slider user_profile_wish margin-b-7">
            <div class="container-fluid">
                @if(!$body['courses']->isEmpty())
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="swip__stories">
                            <div class="swiper-container blog-slider">
                                <div class="title_sections_inner">
                                    <h4>آموزش های نشان شده</h4>
                                </div>
                                <div class="swiper-wrapper">
                                    @foreach($body['courses'] as $course)
                                        <div class="swiper-slide">
                                            <div class="grid_blog_avatar">
                                                <div class="cover_blog">
                                                    <img src="{{$course['image']}}" alt="">
                                                    <div class="icon_played">
                                                        <a href="{{route("{$course['domain']}.academy.course", ["code" => $course['code']])}}" class="btn btn_video">
                                                            <div class="scale rounded-circle b play_video">
                                                                <i class="tio pe-7s-link"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="body_blog">
                                                    <a href="{{url('/academy/course/'.$course['code'])}}">
                                                        <div class="person media">
                                                            <div class="media-body">
                                                                <div class="txt">
                                                                    <h3>{{$course['title']}}</h3>
                                                                    <time><span class="pe-7s-timer"></span> {{gmdate('H:i:s', $course['duration'])}} </time>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="swiper-button-next">
                                    <i class="tio pe-7s-angle-left"></i>
                                </div>
                                <div class="swiper-button-prev">
                                    <i class="tio pe-7s-angle-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="title_sections_inner">
                                <h4>آموزش های نشان شده</h4>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="alert alert-warning fade show" role="alert">
                                 هنوز آموزشی را به لیست علاقه مندی های خود اضافه نکرده ایید.
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </section>--}}
        <!-- End featured courses -->

    </main>
    <!-- End main -->

@endsection

@section('head-script')
    <script type="text/javascript" src="{{ asset('/js/jquery-3.5.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/proper.min.js') }}"></script>
@endsection

