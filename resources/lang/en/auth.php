<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'اطلاعات کاربری شما نامعتبر می باشد.',
    'password' => 'رمز عبور شما اشتباه وارد شده است.',
    'throttle' => 'اقدام های متوالی ورود به حساب کاربری، لطفا به مدت :second صبر کنید.',

];
