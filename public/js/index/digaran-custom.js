// ------------------------------------------------
// Project Name: DigaranBodyArt Coming Soon & Landing Page Template
// Project Description: DigaranBodyArt - clean and bold coming soon & landing page template to kick-start your project
// Author: Awwwat Inc
// Author URI: http://awwwat.com
// File name: digaran-custom.js
// ------------------------------------------------

// ------------------------------------------------
// Table of Contents
// ------------------------------------------------
//
//  1. Loader & Main Section Loading Animation
//  2. Swiper Slider Settings
//  3. Skillbars Settings
//  4. YTPlayer Settings
//  5. Vegas Settings
//  6. KBW-Countdown Settings
//  7. Mailchimp Notify Form
//  8. Say Hello Form
//  9. ParticlesJS Backgrounds
//
// ------------------------------------------------
// Table of Contents End
// ------------------------------------------------

$(window).on("load", function() {

    "use strict";

    // --------------------------------------------- //
    // Loader & Main Section Loading Animation Start
    // --------------------------------------------- //
    $(".loader").addClass('is-animated');

    setTimeout(function(){
        $(".loader").addClass('loaded');
    },200);

    setTimeout(function(){
        $("body").addClass('loaded');
    },200);

    // --------------------------------------------- //
    // Loader & Main Section Loading Animation End
    // --------------------------------------------- //

    // --------------------------------------------- //
    // Swiper Slider Settings Start
    // --------------------------------------------- //
    var sliderSlide = $('.slider-slide'),
        sliderFade = $('.slider-fade');

    if (sliderSlide.length) {
        setTimeout(function(){
            var swiper = new Swiper('.swiper-container', {
                spaceBetween: 0,
                speed: 1000,
                centeredSlides: true,
                loop: true,
                autoplay: {
                    delay: 2000,
                    disableOnInteraction: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        },3200);
    };

    if (sliderFade.length) {
        setTimeout(function(){
            var swiper = new Swiper('.swiper-container', {
                effect: 'fade',
                spaceBetween: 0,
                speed: 1000,
                centeredSlides: true,
                loop: true,
                autoplay: {
                    delay: 2000,
                    disableOnInteraction: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        },3200);
    };
    // --------------------------------------------- //
    // Swiper Slider Settings End
    // --------------------------------------------- //

});

$(function() {

    "use strict";

    // --------------------------------------------- //
    // Skillbars Settings Start
    // --------------------------------------------- //
    $('.skillbar').skillBars({
        from: 0,
        speed: 4000,
        interval: 100,
    });
    // --------------------------------------------- //
    // Skillbars Settings End
    // --------------------------------------------- //

    // --------------------------------------------- //
    // YTPlayer Settings Start
    // --------------------------------------------- //
    var bgndVideo = $("#bgndVideo");

    if(bgndVideo.length){
        bgndVideo.mb_YTPlayer({
            mute: true,
            containment: '#video-wrapper',
            showControls:false,
            autoPlay:true,
            loop:true,
            startAt:0,
            quality:'default'
        });
    };
    // --------------------------------------------- //
    // YTPlayer Settings End
    // --------------------------------------------- //

    // --------------------------------------------- //
    // Vegas Kenburns Start
    // --------------------------------------------- //
    var bgndKenburns = $('#bgndKenburns');
    if(bgndKenburns.length){
        bgndKenburns.vegas({
            timer: false,
            delay: 10000,
            transition: 'fade2',
            transitionDuration: 2000,
            slides: [
                { src: "img/backgrounds/1200x1200-bg-kenburns-1.jpg" },
                { src: "img/backgrounds/1200x1200-bg-kenburns-2.jpg" },
                { src: "img/backgrounds/1200x1200-bg-kenburns-3.jpg" }
            ],
            animation: [ 'kenburnsUp', 'kenburnsDown', 'kenburnsLeft', 'kenburnsRight' ]
        });
    };

    var bgndKenburns2 = $('#bgndKenburns-2');
    if(bgndKenburns2.length){
        bgndKenburns2.vegas({
            timer: false,
            delay: 10000,
            transition: 'fade2',
            transitionDuration: 2000,
            slides: [
                { src: "img/backgrounds/1200x1200-bg-kenburns-4.jpg" },
                { src: "img/backgrounds/1200x1200-bg-kenburns-5.jpg" },
                { src: "img/backgrounds/1200x1200-bg-kenburns-6.jpg" }
            ],
            animation: [ 'kenburnsUp', 'kenburnsDown', 'kenburnsLeft', 'kenburnsRight' ]
        });
    };

    var bgndKenburnsFull = $('#bgndKenburnsFull');
    if(bgndKenburnsFull.length){
        bgndKenburnsFull.vegas({
            timer: false,
            delay: 10000,
            transition: 'fade2',
            transitionDuration: 2000,
            slides: [
                { src: "img/backgrounds/1920x1280-bg-kenburns-1.jpg" },
                { src: "img/backgrounds/1920x1280-bg-kenburns-2.jpg" },
                { src: "img/backgrounds/1920x1280-bg-kenburns-3.jpg" }
            ],
            animation: [ 'kenburnsUp', 'kenburnsDown', 'kenburnsLeft', 'kenburnsRight' ]
        });
    };
    // --------------------------------------------- //
    // Vegas Kenburns End
    // --------------------------------------------- //

    // --------------------------------------------- //
    // KBW-Countdown Start
    // --------------------------------------------- //
    $('#countdown').countdown({until: $.countdown.UTCDate(+10, 2021, 5, 26), format: 'D'});
    $('#countdown-large').countdown({until: $.countdown.UTCDate(+10, 2021, 5, 26), format: 'DHMS'});
    // --------------------------------------------- //
    // KBW-Countdown End
    // --------------------------------------------- //


});
