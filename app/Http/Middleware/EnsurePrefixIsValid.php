<?php

namespace App\Http\Middleware;

use App\Http\Traits\MultimediaTrait;
use App\Models\Category;
use Closure;
use Illuminate\Http\Request;

class EnsurePrefixIsValid
{
    use MultimediaTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $prefix = $request->route()->getPrefix();

        if($prefix == "tattoo"){
            $request->session()->put('prefix','tattoo');
            $this->setCategories('tattoo');
            return $next($request);
        }
        elseif($prefix == "piercing"){
            $request->session()->put('prefix', 'piercing');
            $this->setCategories('piercing');
            return $next($request);
        }
        else{
            return redirect()->route('index');
        }
    }

    public function setCategories($prefix){
        $categories = Category::query()
        ->where('domain', $prefix)
        ->where('type','product')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->orderBy('id','asc')
        ->get();

        session()->put(['categories' => (is_null($categories)) ? [] : $categories->map(function ($attachment){
            return [
                'id' => $attachment['id'],
                'name' => $attachment['name'],
                'image' => $this->urlMaker($attachment->load('image')->image, 'single', 'category')->thumbnail_file_name,
            ];
        })]);

        return;
    }
}
