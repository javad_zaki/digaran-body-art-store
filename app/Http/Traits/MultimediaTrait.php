<?php


namespace App\Http\Traits;


use Illuminate\Support\Facades\App;

trait MultimediaTrait
{

    /**
     * @param $collection
     * @param $entity
     * @return mixed
     */
    public function urlMaker($collection, $type, $entity){

        switch ($type){

            case 'multiple' :
                if(!is_null($collection)){
                    foreach ($collection as $item) {
                        $item->primary_file_name = config('app.cms_url')."/cdn/?entity={$entity}&file={$item->primary_file_name}";
                        $item->thumbnail_file_name = config('app.cms_url')."/cdn/?entity={$entity}&file={$item->thumbnail_file_name}";
                    }
                }
            break;

            case 'single' :
                if(!is_null($collection)){
                    $collection->primary_file_name = config('app.cms_url')."/cdn/?entity={$entity}&file={$collection->primary_file_name}";
                    $collection->thumbnail_file_name = config('app.cms_url')."/cdn/?entity={$entity}&file={$collection->thumbnail_file_name}";
                }
            break;
        }

        return $collection;
    }


}
