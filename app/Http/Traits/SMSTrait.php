<?php


namespace App\Http\Traits;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Kavenegar\KavenegarApi;

trait SMSTrait
{

    public function send($receptor, $message){

        try {
            $api = new KavenegarApi("6C307863555A747662327659454E7548786C4236696C567A5665452B4648777A6E6F6742697846587379673D");
            $response = $api->Send( "10006666060606", $receptor, $message);
            return true;
        } catch (\Kavenegar\Exceptions\ApiException $e) {
            return $e->errorMessage();
        }

    }


    public function lookup($receptor, array $tokens, $template){

        $client = new Client();
        $url = "https://api.kavenegar.com/v1/75392B743076333849664A4C336D61486A475032762B4D4135646A394F6E43554547717135337A714C42513D/verify/lookup.json?";

        switch ($template){
            case 'verify':
                $request = "receptor={$receptor}&token={$tokens[0]}&template=verify";
                break;

            case 'reset-password':
                $request = "receptor={$receptor}&token={$tokens[0]}&template=reset-password";
                break;

            default:
                $request = "";
                break;
        }

        try {

            $response = $client->request(
                'GET',
                $url.$request,
                [
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ]);

            return true;

        } catch (ClientException $e) {
            return false;
        }

    }



}
