<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\SMSTrait;
use App\Models\Verification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class PasswordResetLinkController extends Controller
{

    use SMSTrait;

    /**
     * Display the password reset link request view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.forgot-password');
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {


        $rules = ['mobile' => 'required|string|min:11|max:11|exists:users'];
        $this->validate($request, $rules, config('validation.verification.messages'));

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        /*$status = Password::sendResetLink(
            $request->only('mobile')
        );

        return $status == Password::RESET_LINK_SENT
                    ? back()->with('status', __($status))
                    : back()->withInput($request->only('mobile'))
                            ->withErrors(['mobile' => __($status)]);*/

        $verification =  Verification::create([
            'mobile'    => $request->mobile,
            'code'      => mt_rand(1111,9999),
            'expire_at' => Carbon::now()->addMinutes(5),
        ]);

        $this->lookup($verification->mobile, [$verification->code],"reset-password");

        return redirect('/reset-password')->with('message', 'کد بازنشانی به شماره موبایل شما ارسال شد.');

    }


}
