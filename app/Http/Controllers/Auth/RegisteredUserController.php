<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\SMSTrait;
use App\Models\User;
use App\Models\Verification;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{

    use SMSTrait;

    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Display the verification view.
     *
     * @return \Illuminate\View\View
     */
    public function verification()
    {
        return view('auth.verification');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sendVerificationCode(Request $request)
    {

        ($request->unique == 1)? $rules = ['mobile' => 'required|string|min:11|max:11|unique:users'] : $rules = ['mobile' => 'required|string|min:11|max:11|exists:users'];
        $this->validate($request, $rules, config('validation.verification.messages'));

        try {

            $verification =  Verification::create([
                'mobile'    => $request->mobile,
                'code'      => mt_rand(1111,9999),
                'expire_at' => Carbon::now()->addMinutes(5),
            ]);

            $this->lookup($verification->mobile, [$verification->code],"verify");

            return redirect('/register');

        }
        catch (\Exception $e){

            return redirect('/register/verification');
        }
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'code'     => 'required|string|max:4',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $verification = Verification::query()->where('code', $request->code)->where('expire_at', '>', Carbon::now())->first();
        if(is_null($verification)) return redirect('/register/verification');

        Auth::login($user = User::create([
            'mobile' => $verification->mobile,
            'password' => Hash::make($request->password),
        ]));

        $verification->forceDelete();

        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME);
    }
}
