<?php

namespace App\Http\Controllers;

use App\Http\Traits\MultimediaTrait;
use App\Http\Traits\RequestTrait;
use App\Models\Course;
use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    use MultimediaTrait;

    public function index(Request $request){

        $expr = $request->expr;
        $prefix = session()->get('prefix');

        $products = Product::with('category')
        ->where(['domain' => $prefix, 'status' => 'enable'])
        ->where(function($query) use ($expr) {
            return $query
                ->where('name', 'LIKE', "%{$expr}%")
                ->orWhere('short_description', 'LIKE', "%{$expr}%")
                ->orWhere('technical_description', 'LIKE', "%{$expr}%");
        })
        ->get();

       /* $courses = Course::with('category')
        ->where('domain', $prefix)
        ->where('status', 'enable')
        ->where('title', 'LIKE', "%{$expr}%")
        ->orWhere('subtitle', 'LIKE', "%{$expr}%")
        ->orWhere('short_description', 'LIKE', "%{$expr}%")
        ->orWhere('tags', 'LIKE', "%{$expr}%")
        ->whereNull('deleted_at')->get();*/

        return view('search',
            ['body' => [
                'products' => (is_null($products)) ? [] : $products->map(function ($attachment){
                    return [
                        'sku' => $attachment['sku'],
                        'prefix' => $attachment['domain'],
                        'name' => $attachment['name'],
                        'base_price' => $attachment['base_price'],
                        'category' => $attachment['category']['name'],
                        'price_after_discount' => $attachment['price_after_discount'],
                        'image' => $this->urlMaker($attachment->load('primary')->primary, 'single', 'product')->thumbnail_file_name,
                    ];
                }),
                /*'courses' => (is_null($courses)) ? [] : $courses->map(function ($attachment){
                    return [
                        'code' => $attachment['code'],
                        'prefix' => $attachment['domain'],
                        'category' => $attachment['category']['name'],
                        'title' => $attachment['title'],
                        'duration' => $attachment['duration'],
                        'image' => $this->urlMaker($attachment->load('cover')->cover, 'single', 'course')->thumbnail_file_name,
                    ];
                }),*/
            ]]
        );

    }
}
