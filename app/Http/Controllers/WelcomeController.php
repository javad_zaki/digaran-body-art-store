<?php

namespace App\Http\Controllers;

use App\Http\Traits\MultimediaTrait;
use App\Models\Artist;
use App\Models\Content;
use App\Models\Course;
use App\Models\Product;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    use MultimediaTrait;


    public function index(Request $request){

        $prefix = $request->route()->getPrefix();

        $slides = Content::query()
        ->where('domain', $prefix)
        ->where('type','slider')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->get();

        $promos = Content::query()
        ->where('domain', $prefix)
        ->where('type','promo')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->get();

        $products = Product::query()->with('category')
        ->where('domain', $prefix)
        ->where('type','featured')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->get();

        $courses = Course::query()->with('artist')
        ->where('domain', $prefix)
        ->where('type','featured')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->get();

        $artists = Artist::query()
        ->where('domain', $prefix)
        ->where('status', 'enable')
        ->where('role', 'artist')
        ->whereNull('deleted_at')
        ->get();


        return view('welcome',
            ['body' => [
                'prefix' => $prefix,
                'slides' => (is_null($slides)) ? [] : $slides->map(function ($attachment){
                    $detail = json_decode($attachment->detail, true);
                    return [
                        'image' => $this->urlMaker($attachment->load('sliderImage')->sliderImage, 'single', 'slider')->primary_file_name,
                        'title' => $detail['title'],
                        'link' => $detail['link'],
                    ];
                }),
                'promos' => (is_null($promos)) ? [] : $promos->map(function ($attachment){
                    $detail = json_decode($attachment->detail, true);
                    return [
                        'image' => $this->urlMaker($attachment->load('promoImage')->promoImage, 'single', 'promo')->primary_file_name,
                        'link' => $detail['link'],
                    ];
                }),
                'products' => (is_null($products)) ? [] : $products->map(function ($attachment){
                    return [
                        'sku' => $attachment['sku'],
                        'name' => $attachment['name'],
                        'base_price' => $attachment['base_price'],
                        'discount'   => $attachment['discount'],
                        'category' => $attachment['category']['name'],
                        'price_after_discount' => $attachment['price_after_discount'],
                        'image' => $this->urlMaker($attachment->load('primary')->primary, 'single', 'product')->thumbnail_file_name,
                    ];
                }),
                'courses' => (is_null($courses)) ? [] : $courses->map(function ($attachment){

                    return [
                        'code' => $attachment['code'],
                        'title' => $attachment['title'],
                        'duration' => $attachment['duration'],
                        'image' => $this->urlMaker($attachment->load('cover')->cover, 'single', 'course')->thumbnail_file_name,
                    ];
                }),
                'artists' => (is_null($artists)) ? [] : $artists->map(function ($attachment){
                    return [
                        'name' => $attachment['name'],
                        'biography' => $attachment['biography'],
                        'social' => json_decode($attachment['social'], true),
                        'image' => $this->urlMaker($attachment->load('avatar')->avatar, 'single', 'artist')->primary_file_name,
                    ];
                }),
            ]]
        );
    }
}
