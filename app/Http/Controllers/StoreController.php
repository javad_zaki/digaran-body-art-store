<?php

namespace App\Http\Controllers;

use App\Http\Traits\MultimediaTrait;
use App\Models\Category;
use App\Models\Product;
use App\Models\Wish;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    use MultimediaTrait;

    public function index(Request $request){

        $prefix = $request->route()->getPrefix();

        $products = Product::query()
        ->where('domain', $prefix)
        ->where('status', 'enable')
        ->whereNull('deleted_at');

       /* if(isset($request->expr)){
            $expr = $request->expr;
            $products->where('name', 'LIKE', "%{$expr}%");
            $products->orWhere('short_description', 'LIKE', "%{$expr}%");
            $products->orWhere('technical_description', 'LIKE', "%{$expr}%");
        }*/

        if(isset($request->category)) $products->where('category_id',  $request->category);

        $products = $products->paginate(6);

        /*$categories = Category::query()
        ->where('domain', $prefix)
        ->where('type','product')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->get();*/

        return view('store.store',
            ['body' => [
                'prefix' => $prefix,
                'category' => $request->category,
                'products' => (is_null($products)) ? [] : $products->map(function ($attachment){
                    return [
                        'sku' => $attachment['sku'],
                        'name' => $attachment['name'],
                        'base_price' => $attachment['base_price'],
                        'discount' => $attachment['discount'],
                        'category' => $attachment['category']['name'],
                        'price_after_discount' => $attachment['price_after_discount'],
                        'image' => $this->urlMaker($attachment->load('primary')->primary, 'single', 'product')->thumbnail_file_name,
                    ];
                }),
                'pagination' =>[
                    'total' => $products->total(),
                    'last_page' => $products->lastPage(),
                    'current_page' => $products->currentPage(),
                ],
                /*'categories' => (is_null($categories)) ? [] : $categories->map(function ($attachment){
                    return [
                        'id' => $attachment['id'],
                        'name' => $attachment['name'],
                        'image' => $this->urlMaker($attachment->load('image')->image, 'single', 'category')->thumbnail_file_name,
                    ];
                }),*/
            ]]
        );

    }

    public function product(Request $request){

        try {
            $prefix = $request->route()->getPrefix();
            $wish = null;

            $product = Product::query()->with('category')
            ->where('domain',$prefix)
            ->where('sku', $request->sku)
            ->where('status', 'enable')
            ->whereNull('deleted_at')
            ->first();

            if(is_null($product)) return redirect()->back();

            $image = $this->urlMaker($product->load('primary')->primary, 'single','product');
            $colors = $this->urlMaker($product->load('color')->color, 'multiple','product');
            $catalog = $this->urlMaker($product->load('catalog')->catalog, 'multiple','product');

            if(auth()->check()){
                $wish = Wish::query()
                ->where('user_id', auth()->user()->id)
                ->where('wishes_type', 'App\Models\Product')
                ->where('wishes_id', $product->id)
                ->where('domain', $prefix)
                ->whereNull('deleted_at')
                ->first();
            }

            return view('store.product', ['body' =>
                ['specifications' =>
                    [
                        'category' => $product['category']['name'],
                        'category_id' => $product['category']['id'],
                        'id' => $product['id'],
                        'sku' => $product['sku'],
                        'qty' => $product['qty'],
                        'name' => $product['name'],
                        'short_description' => $product['short_description'],
                        'properties' => json_decode($product['properties'], true),
                        'price' => [
                            'base_price' => $product['base_price'],
                            'price_after_discount' => $product['price_after_discount'],
                            'discount' => $product['discount']
                        ],
                    ],
                    'prefix' => $prefix,
                    'image'  => $image,
                    'colors' => $colors,
                    'catalog' => $catalog,
                    'description' => $product['technical_description'],
                    'wish' => (!is_null($wish)? 1: 0)
                ],
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }
    }

    public function wish(Request $request){

        try {
            $prefix = $request->route()->getPrefix();

            $wish = Wish::query()
            ->where('user_id', auth()->user()->id)
            ->where('wishes_type', 'App\Models\Product')
            ->where('wishes_id', $request->id)
            ->where('domain', $prefix)
            ->whereNull('deleted_at')
            ->first();

            if(!is_null($wish)){
                $request->session()->flash('message', config('message.product.duplicated'));
                return redirect()->back();
            }

            $wish = new Wish();
            $wish->user_id = auth()->user()->id;
            $wish->wishes_type = 'App\Models\Product';
            $wish->wishes_id = $request->id;
            $wish->domain = $prefix;
            $wish->save();

            $request->session()->flash('message', config('message.product.wished'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }
    }

    public function wishNot(Request $request){

        try {
            $prefix = $request->route()->getPrefix();

            $wish = Wish::query()
            ->where('user_id', auth()->user()->id)
            ->where('wishes_type', 'App\Models\Product')
            ->where('wishes_id', $request->id)
            ->where('domain', $prefix)
            ->whereNull('deleted_at')
            ->first();

            $wish->delete();

            $request->session()->flash('message', config('message.product.unwished'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }
    }

}
