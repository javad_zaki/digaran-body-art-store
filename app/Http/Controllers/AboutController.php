<?php

namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(){
        $about = Content::query()
        ->where('type', 'about')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->first();
        return view('about', ['body' => ['about' => json_decode($about['detail'], true)]]);
    }
}
