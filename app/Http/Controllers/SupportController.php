<?php

namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function index(){

        $support = Content::query()
        ->where('type', 'support')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->first();

        return view('support', ['body' => ['support' => json_decode($support['detail'], true)]]);
    }
}
