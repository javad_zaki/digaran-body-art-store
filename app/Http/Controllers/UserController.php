<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Traits\MultimediaTrait;
use App\Http\Traits\RequestTrait;
use App\Models\Order;
use App\Models\User;
use App\Models\Wish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use MultimediaTrait;

    public function index(Request $request){

        try {

            $prefix = $request->route()->getPrefix();

            $user = User::find(auth()->user()->id);
            $orders = Order::query()->where('user_id', $user->id)->get();
            $products = Wish::query()->where('user_id', $user->id)->where('wishes_type','App\Models\Product')->whereNull('deleted_at')->get();
            /*$courses = Wish::query()->where('user_id', $user->id)->where('wishes_type','App\Models\Course')->whereNull('deleted_at')->get();*/

            return view('user.profile', [
                'body' => [
                    'prefix' => $prefix,
                    'user' => [
                        'name' => $user->name,
                        'national_id' => $user->national_id,
                        'mobile' => $user->mobile,
                        'email' => $user->email,
                        'postal_code' => $user->postal_code,
                        'province' => $user->province,
                        'city' => $user->city,
                        'address' => $user->address,
                    ],
                    'orders' => (is_null($orders)) ? [] : $orders->map(function ($attachment){
                        return [
                            'code' => $attachment['code'],
                            'qty' => $attachment['qty'],
                            'subtotal' => $attachment['subtotal'],
                            'shipping' => $attachment['shipping'],
                            'discount' => $attachment['discount'],
                            'tax' => $attachment['tax'],
                            'total' => $attachment['total'],
                            'payable' => $attachment['payable'],
                            'province' => $attachment['province'],
                            'city' => $attachment['city'],
                            'postal_code' => $attachment['postal_code'],
                            'address' => $attachment['address'],
                            'description' => $attachment['description'],
                            'status' => $attachment['status'],
                            'send_paper_invoice' => $attachment['send_paper_invoice'],
                            'updated_at' => $attachment['updated_at'],
                            'items' => json_decode($attachment->items, true),
                        ];
                    }),
                    'products' => (is_null($products)) ? [] : $products->map(function ($attachment){
                        return [
                            'sku' => $attachment['wishable']['sku'],
                            'domain' => $attachment['wishable']['domain'],
                            'name' => $attachment['wishable']['name'],
                            'image' => $this->urlMaker($attachment['wishable']->load('primary')->primary, 'single', 'product')->thumbnail_file_name,
                        ];
                    }),
                    /*'courses' => (is_null($courses)) ? [] : $courses->map(function ($attachment){
                        return [
                            'code' => $attachment['wishable']['code'],
                            'title' => $attachment['wishable']['title'],
                            'duration' => $attachment['wishable']['duration'],
                            'domain' => $attachment['wishable']['domain'],
                            'image' => $this->urlMaker($attachment['wishable']->load('cover')->cover, 'single', 'course')->thumbnail_file_name,
                        ];
                    }),*/
                ]
            ]);

        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }

    }

    public function password(){
        return view('user.password');
    }

    public function updatePassword(Request $request){

        $request->validate(config('validation.change_password.rules'),config('validation.change_password.messages'));

        try {
            $user = User::find(auth()->user()->id);
            if(!Hash::check($request->current_password, $user->password)){
                $request->session()->flash('message', config('message.password.equality'));
                return redirect()->back();
            }

            $user->password = Hash::make($request->password);
            $user->save();

            Auth::guard('web')->logout();

            /*$request->session()->invalidate();*/

            $request->session()->regenerateToken();

            return redirect('/login');

        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){

        $request->validate([
            'name' => ['required', new \App\Rules\PersianAlpha()],
            'national_id' => 'required|min:10|max:10',
            'province' => ['required', new \App\Rules\PersianAlpha()],
            'city' => ['required', new \App\Rules\PersianAlpha()],
            'email' => 'required|string',
            'address' => ['required', new \App\Rules\PersianAlpha()],
            'postal_code' => 'required|min:10|max:10',
        ],config('validation.user.messages'));

        try {
            $user = User::find(auth()->user()->id);

            $user->name = $request->name;
            $user->national_id = $request->national_id;
            $user->email = $request->email;
            $user->postal_code = $request->postal_code;
            $user->province = $request->province;
            $user->city = $request->city;
            $user->address = $request->address;
            $user->save();

            return redirect('/user');

        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }

    }
}
