<?php

namespace App\Http\Controllers;

use App\Http\Traits\MultimediaTrait;
use App\Http\Traits\RequestTrait;
use App\Models\Category;
use App\Models\Course;
use App\Models\Wish;
use Illuminate\Http\Request;

class AcademyController extends Controller
{
    use RequestTrait, MultimediaTrait;

    public function index(Request $request){

        try {

            $prefix = $request->route()->getPrefix();

            $courses = Course::with('category')
                ->where('domain', $prefix)
                ->where('status', 'enable')
                ->whereNull('deleted_at');

            if(isset($request->expr)){
                $expr = $request->expr;
                $courses->where('title', 'LIKE', "%{$expr}%");
                $courses->orWhere('subtitle', 'LIKE', "%{$expr}%");
                $courses->orWhere('short_description', 'LIKE', "%{$expr}%");
                $courses->orWhere('tags', 'LIKE', "%{$expr}%");
            }

            if(isset($request->category)) $courses->where('category_id',  $request->category);

            $courses = $courses->paginate(6);

            $categories = Category::query()
                ->where('domain', $prefix)
                ->where('type','course')
                ->where('status', 'enable')
                ->whereNull('deleted_at')
                ->get();

            return view('academy.academy',
                ['body' => [
                    'prefix' => $prefix,
                    'courses' => (is_null($courses)) ? [] : $courses->map(function ($attachment){
                        return [
                            'code' => $attachment['code'],
                            'category' => $attachment['category']['name'],
                            'title' => $attachment['title'],
                            'duration' => $attachment['duration'],
                            'image' => $this->urlMaker($attachment->load('cover')->cover, 'single', 'course')->thumbnail_file_name,
                        ];
                    }),
                    'pagination' =>[
                        'total' => $courses->total(),
                        'last_page' => $courses->lastPage(),
                        'current_page' => $courses->currentPage(),
                    ],
                    'categories' => (is_null($categories)) ? [] : $categories->map(function ($attachment){
                        return [
                            'id' => $attachment['id'],
                            'name' => $attachment['name'],
                        ];
                    }),
                ]]
            );

        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }

    }

    public function course(Request $request){

        try {

            $prefix = $request->route()->getPrefix();
            $wish = null;

            $course = Course::query()->with(['artist','category'])
            ->where('domain',$prefix)
            ->where('code', $request->code)
            ->where('status', 'enable')
            ->whereNull('deleted_at')
            ->first();

            if(is_null($course)) return redirect()->back();

            $course['artist']['avatar'] = $this->urlMaker($course['artist']->load('avatar')->avatar, 'single', 'artist');
            $course['cover'] = $this->urlMaker($course->load('cover')->cover, 'single', 'course');

            if(auth()->check()){
                $wish = Wish::query()
                ->where('user_id', auth()->user()->id)
                ->where('wishes_type', 'App\Models\Course')
                ->where('wishes_id', $course->id)
                ->where('domain', $prefix)
                ->whereNull('deleted_at')
                ->first();
            }

            return view('academy.course', ['body' =>
                ['course' =>
                    [
                        'id' => $course['id'],
                        'category' => $course['category']['name'],
                        'artist' => $course['artist'],
                        'cover' => $course['cover'],
                        'video_link' => $course['video_link'],
                        'title' => $course['title'],
                        'subtitle' => $course['subtitle'],
                        'duration' => gmdate("H:i:s",$course['duration']),
                        'short_description' => $course['short_description'],
                        'tags' => explode(',', $course['tags']),
                        'wish' => (!is_null($wish)? 1: 0)
                    ],
                    'prefix' => $prefix,
                ],
            ]);

        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }
    }


    public function wish(Request $request){

        try {
            $prefix = $request->route()->getPrefix();

            $wish = Wish::query()
            ->where('user_id', auth()->user()->id)
            ->where('wishes_type', 'App\Models\Course')
            ->where('wishes_id', $request->id)
            ->where('domain', $prefix)
            ->whereNull('deleted_at')
            ->first();

            if(!is_null($wish)){
                $request->session()->flash('message', config('message.course.duplicated'));
                return redirect()->back();
            }

            $wish = new Wish();
            $wish->user_id = auth()->user()->id;
            $wish->wishes_type = 'App\Models\Course';
            $wish->wishes_id = $request->id;
            $wish->domain = $prefix;
            $wish->save();

            $request->session()->flash('message', config('message.course.wished'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }
    }

    public function wishNot(Request $request){

        try {
            $prefix = $request->route()->getPrefix();

            $wish = Wish::query()
                ->where('user_id', auth()->user()->id)
                ->where('wishes_type', 'App\Models\Course')
                ->where('wishes_id', $request->id)
                ->where('domain', $prefix)
                ->whereNull('deleted_at')
                ->first();

            $wish->delete();

            $request->session()->flash('message', config('message.course.unwished'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }
    }
}
