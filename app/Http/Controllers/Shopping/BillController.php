<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class BillController extends Controller
{
    public function index(){

        $tax = Content::query()
        ->where('type', 'tax')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->first();

        $shipping = Content::query()
        ->where('type', 'shipping')
        ->where('status', 'enable')
        ->whereNull('deleted_at')
        ->first();


        return view('shopping.bill', ['body' => ['tax' => json_decode($tax, true), 'shipping' => json_decode($shipping,'true')]]);
    }
}
