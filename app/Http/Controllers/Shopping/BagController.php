<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Http\Traits\MultimediaTrait;
use App\Models\Content;
use App\Models\Product;
use Illuminate\Http\Request;

class BagController extends Controller
{

    use MultimediaTrait;

    public function index(){
        return view('shopping.bag');
    }


    public function add(Request $request){

        $product = Product::query()->where('sku',$request->sku)->first();

        if($product->qty == 0){
            $request->session()->flash('message', 'این محصول موجود نمی باشد.');
            return redirect()->back();
        }

        $image = $this->urlMaker($product->load('primary')->primary, 'single', 'product')->thumbnail_file_name;
        $colors = $product->load('color')->color;
        foreach ($colors as $key => $color){
            $colors [$key] = json_decode($color->property, true)['color'];
        }

        if(!is_null($product)){

            if ($request->session()->exists('bag')){
                foreach (session('bag.items') as $item){
                    if($item['sku'] == $product->sku){
                        $request->session()->flash('message', config('message.bag.duplicated'));
                        return redirect()->back();
                    }
                }
            }

            $request->session()->push('bag.items',
                [
                    'sku' => $product->sku,
                    'name' => $product->name,
                    'image' => $image,
                    'base_price' => $product['base_price'],
                    'price_after_discount' => $product['price_after_discount'],
                    'discount' => $product['discount'],
                    'qty' => '1',
                    'colors' => $colors,
                    'stoke' => $product->qty,
                    'unavailable' => false,
                    'total' => $product['price_after_discount']
                ]
            );

            $request->session()->flash('message', config('message.bag.added'));
        }


        return redirect()->back();
    }

    public function remove(Request $request){

        if ($request->session()->has('bag.items')){
            foreach (session('bag.items') as $key => $item){
                if($item['sku'] == $request->sku){
                    $request->session()->pull('bag.items.'.$key);
                    $request->session()->flash('message', config('message.bag.removed'));
                }
            }
        }

        return redirect()->back();
    }

    public function confirm(Request $request){
        try {

            if ($request->session()->has('bag.items')){
                $requested_items = $request->all();
                $unavailable = false;

                foreach (session('bag.items') as $key => $item){
                    if((int)$requested_items[$item['sku']] > $item['stoke']){
                        $item['unavailable'] = true;
                        $unavailable = true;
                    }
                    session()->put('bag.items.'.$key,$item);
                }

                if($unavailable){
                    return redirect('shopping/bag');
                }


                $subtotal = 0;
                $qty = 0;
                $totalTaxCost = 0;

                $shipping = Content::query()
                ->where('type', 'shipping')
                ->where('status', 'enable')
                ->whereNull('deleted_at')->first();
                $shipping = json_decode($shipping['detail'], true)['cost'];

                $tax = Content::query()
                ->where('type', 'tax')
                ->where('status', 'enable')
                ->whereNull('deleted_at')->first();
                $tax = json_decode($tax['detail'], true)['tax'];

                foreach (session('bag.items') as $key => $item){
                    $item['qty'] = $requested_items[$item['sku']];
                    $item['total'] = $requested_items[$item['sku']] * $item['price_after_discount'];
                    $subtotal += $item['total'];
                    $qty += $item['qty'];
                    $totalTaxCost += (($item['price_after_discount'] * $tax) / 100) * $item['qty'];

                    session()->put('bag.items.'.$key,$item);
                }

                $total = $subtotal + $shipping + $totalTaxCost;

                $request->session()->put('bag.invoice',
                    [
                        'qty' => $qty,
                        'subtotal' => $subtotal,
                        'shipping' => $shipping,
                        'tax' => $totalTaxCost,
                        'total' => $total,
                    ]
                );
            }
            return redirect()->action([CheckoutController::class, 'index']);
        }
        catch (\Exception $e){
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }
    }

}
