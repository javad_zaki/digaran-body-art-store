<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Models\Guest;
use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{
    public function index(){

        try {

            $order = Order::create([
                'code' => mt_rand(11111111,99999999),
                'user_id' => (auth()->check()) ? auth()->user()->id : null,
                'user_type' => (auth()->check()) ? 'member' : 'guest',
                'items' => json_encode(session()->get('bag.items')),
                'qty' => session()->get('bag.invoice')['qty'],
                'subtotal' => session()->get('bag.invoice')['subtotal'],
                'shipping' => session()->get('bag.invoice')['shipping'],
                'discount' => 0,
                'tax' => session()->get('bag.invoice')['tax'],
                'total' => session()->get('bag.invoice')['total'],
                'payable' => session()->get('bag.invoice')['total'],
                'province' => session()->get('bag.shipping')['province'],
                'city' => session()->get('bag.shipping')['city'],
                'postal_code' => session()->get('bag.shipping')['postal_code'],
                'address' => session()->get('bag.shipping')['address'],
                'description' => session()->get('bag.shipping')['description'],
                'send_paper_invoice' => true,
            ]);


            $this->createGuest($order);
            $this->createTransaction($order);
            $this->forgetSession();

            return redirect('/shopping/transaction')->with('message', 'سفارش شما با موفقیت ثبت شد. تغییر وضعیت سفارش از طریق پیامک به شما اعلام خواهد شد.');

        }
        catch (\Exception $e){
            return redirect('/shopping/transaction')->with('message', 'ثبت سفارش با مشکل مواجه شده است.');
        }

    }


    public function transaction(){
        return view('shopping.transaction');
    }


    private function createGuest($order){

        try {
            if(auth()->check()){
                if(session()->get('bag.shipping')['recipient'] == 'other'){
                    $recipient = "other";
                }
            }
            else{
                $recipient = "guest";
            }
            Guest::create([
                'order_id' => $order->id,
                'name' => session()->get('bag.shipping')['name'],
                'national_id' => session()->get('bag.shipping')['national_id'],
                'mobile' => session()->get('bag.shipping')['mobile'],
                'province' => session()->get('bag.shipping')['province'],
                'city' => session()->get('bag.shipping')['city'],
                'postal_code' => session()->get('bag.shipping')['postal_code'],
                'address' => session()->get('bag.shipping')['address'],
                'recipient' => $recipient,
            ]);

            return;
        }
        catch (\Exception $e){
            return redirect('/shopping/transaction')->with('message', 'ثبت سفارش با مشکل مواجه شده است.');
        }
    }


    public function createTransaction($order){

        try {
            Transaction::create([
                'order_id' => $order->id,
                'res_num' => 0,
                'ref_num' => 0,
                'trace_num' => 0,
                'customer_ref_num' => 0,
                'mid' => 0,
                'card_hash_pan' => 0,
                'card_mask_pan' => 0,
                'good_ref_id' => 0,
                'merchant_data' => 0,
                'transaction_amount' => 0,
                'state' => 'ok',
            ]);

            return;
        }
        catch (\Exception $e){
            return redirect('/shopping/transaction')->with('message', 'ثبت سفارش با مشکل مواجه شده است.');
        }

    }


    private function forgetSession(){
        try {
            Session::forget('bag');
            return;
        }
        catch (\Exception $e){
            return redirect('/shopping/transaction')->with('message', 'ثبت سفارش با مشکل مواجه شده است.');
        }
    }



}
