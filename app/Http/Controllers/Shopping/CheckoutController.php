<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Models\Guest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CheckoutController extends Controller
{
    public function index(){
        return view('shopping.checkout');
    }

    public function recipient(Request $request){

        $this->validate($request, [
            'recipient' => 'required|in:guest,me,other',
            'name' => ['required', new \App\Rules\PersianAlpha()],
            'national_id' => 'required|min:10|max:10',
            'mobile' => 'required|min:11|max:11',
            'province' => ['required', new \App\Rules\PersianAlpha()],
            'city' => ['required', new \App\Rules\PersianAlpha()],
            'address' => ['required', new \App\Rules\PersianAlpha()],
            'postal_code' => 'required|min:10|max:10',
            'description' => ['bail','nullable',new \App\Rules\PersianAlpha()],
        ], config('validation.checkout.messages'));

        if(\auth()->check() && $request->recipient == 'me'){
            $user = User::find(\auth()->user()['id']);
            $this->validate($request,
            [
                'national_id' => Rule::unique('users')->ignore($user->id),
            ],
            [
                'national_id.unique' => 'این شماره ملی توسط کاربر دیگری استفاده شده است.',
            ]);
        }

        try {

            $request->session()->put('bag.shipping',
                [
                    'name' => $request->name,
                    'national_id' => $request->national_id,
                    'mobile' => $request->mobile,
                    'province' => $request->province,
                    'city' => $request->city,
                    'postal_code' => $request->postal_code,
                    'address' => $request->address,
                    'recipient' => $request->recipient,
                    'description' => $request->description,
                ]
            );

            return redirect()->action([BillController::class, 'index']);

        } catch (\Exception $e) {
            $request->session()->flash('message', config('message.exception.failed'));
            return redirect()->back();
        }

    }
}
