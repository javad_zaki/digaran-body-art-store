<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function sliderImage()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia')->where('type', 'slider');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function promoImage()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia')->where('type', 'promo');
    }
}
