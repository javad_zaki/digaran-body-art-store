<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'res_num',
        'ref_num',
        'trace_num',
        'customer_ref_num',
        'mid',
        'card_hash_pan',
        'card_mask_pan',
        'good_ref_id',
        'merchant_data',
        'transaction_amount',
        'state',
    ];
}
