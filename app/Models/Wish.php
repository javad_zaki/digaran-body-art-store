<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wish extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * Get the parent wishable model (product or course).
     */
    public function wishable()
    {
        return $this->morphTo(__FUNCTION__, 'wishes_type', 'wishes_id');
    }
}
