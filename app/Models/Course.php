<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    /**
     * Get the course that owns the artist.
     */
    public function artist(){
        return $this->belongsTo('App\Models\Artist');
    }

    /**
     * Get the course that owns the category.
     */
    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function cover()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia')->where('type','=','cover');
    }

}
