<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'user_id',
        'user_type',
        'items',
        'qty',
        'subtotal',
        'shipping',
        'discount',
        'tax',
        'total',
        'payable',
        'province',
        'city',
        'postal_code',
        'address',
        'description',
        'status',
        'send_paper_invoice',
    ];

}
