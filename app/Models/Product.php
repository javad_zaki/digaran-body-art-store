<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * Get the product that owns the category.
     */
    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function primary()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia')->where('type','=','image');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function color()
    {
        return $this->morphMany('App\Models\Multimedia', 'multimedia')->where('type','=','color');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function catalog()
    {
        return $this->morphMany('App\Models\Multimedia', 'multimedia')->where('type','=','catalog');
    }
}
