<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\AcademyController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Shopping\BagController;
use App\Http\Controllers\Shopping\BillController;
use App\Http\Controllers\Shopping\CheckoutController;
use App\Http\Controllers\Shopping\PaymentController;
use App\Http\Controllers\SearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [IndexController::class, 'index'])->name('index');
Route::get('/search', [SearchController::class, 'index'])->name('search');
Route::get('/support', [SupportController::class, 'index'])->name('support');
Route::get('/about', [AboutController::class, 'index'])->name('about');

Route::prefix('user')->group(function () {

    Route::get('/', [UserController::class, 'index'])->middleware('auth')->name('user');
    Route::post('/update', [UserController::class, 'update'])->middleware('auth')->name('user.update');
    Route::get('/password', [UserController::class, 'password'])->middleware('auth')->name('user.password');
    Route::post('/password/update', [UserController::class, 'updatePassword'])->middleware('auth')->name('user.password.update');

});

Route::prefix('tattoo')->group(function () {

    Route::get('/', [WelcomeController::class, 'index'])->middleware('prefix')->name('tattoo');
    Route::get('/store', [StoreController::class, 'index'])->middleware('prefix')->name('tattoo.store');
    Route::get('/store/product/{sku}', [StoreController::class, 'product'])->middleware('prefix')->name('tattoo.store.product');
    Route::get('/store/product/wish/{id}', [StoreController::class, 'wish'])->middleware(['prefix','auth'])->name('tattoo.store.product.wish');
    Route::get('/store/product/wishNot/{id}', [StoreController::class, 'wishNot'])->middleware(['prefix','auth'])->name('tattoo.store.product.wishNot');

    Route::get('/academy', [AcademyController::class, 'index'])->middleware('prefix')->name('tattoo.academy');
    Route::get('/academy/course/{code}', [AcademyController::class, 'course'])->middleware('prefix')->name('tattoo.academy.course');
    Route::get('/academy/course/wish/{id}', [AcademyController::class, 'wish'])->middleware(['prefix','auth'])->name('tattoo.academy.course.wish');
    Route::get('/academy/course/wishNot/{id}', [AcademyController::class, 'wishNot'])->middleware(['prefix','auth'])->name('tattoo.academy.course.wishNot');

});

Route::prefix('piercing')->group(function () {

    Route::get('/', [WelcomeController::class, 'index'])->middleware('prefix')->name('piercing');
    Route::get('/store', [StoreController::class, 'index'])->middleware('prefix')->name('piercing.store');
    Route::get('/store/product/{sku}', [StoreController::class, 'product'])->middleware('prefix')->name('piercing.store.product');
    Route::get('/store/product/wish/{id}', [StoreController::class, 'wish'])->middleware('prefix')->name('piercing.store.product.wish');
    Route::get('/store/product/wishNot/{id}', [StoreController::class, 'wishNot'])->middleware('prefix')->name('piercing.store.product.wishNot');

    Route::get('/academy', [AcademyController::class, 'index'])->middleware('prefix')->name('piercing.academy');
    Route::get('/academy/course/{code}', [AcademyController::class, 'index'])->middleware('prefix')->name('piercing.academy.course');
    Route::get('/academy/course/wish/{id}', [AcademyController::class, 'wish'])->middleware('prefix')->name('piercing.academy.course.wish');
    Route::get('/academy/course/wishNot/{id}', [AcademyController::class, 'wishNot'])->middleware('prefix')->name('piercing.academy.course.wishNot');

});

Route::prefix('shopping')->group(function () {

    Route::get('/bag', [BagController::class, 'index'])->name('shopping.bag');
    Route::get('/bag/add/{sku}', [BagController::class, 'add'])->name('shopping.bag.add');
    Route::get('/bag/remove/{sku}', [BagController::class, 'remove'])->name('shopping.bag.remove');
    Route::post('/bag/confirm', [BagController::class, 'confirm'])->name('shopping.confirm');
    Route::get('/checkout', [CheckoutController::class, 'index'])->name('shopping.checkout');
    Route::post('/checkout/recipient', [CheckoutController::class, 'recipient'])->name('shopping.recipient');
    Route::get('/bill', [BillController::class, 'index'])->name('shopping.bill');
    Route::get('/payment', [PaymentController::class, 'index'])->name('shopping.payment');
    Route::get('/transaction', [PaymentController::class, 'transaction'])->name('shopping.transaction');

});


Route::get('/route-cache', function() {
    \Illuminate\Support\Facades\Artisan::call('route:cache');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    return 'cleared';
});

require __DIR__.'/auth.php';

