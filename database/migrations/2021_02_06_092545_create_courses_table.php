<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->foreignId('category_id')->constrained('categories');
            $table->foreignId('artist_id')->constrained('artists');
            $table->string('title');
            $table->string('subtitle');
            $table->integer('duration');
            $table->mediumText('short_description');
            $table->mediumText('tags');
            $table->mediumText('video_link');
            $table->enum('type', ['general','featured'])->default('general');
            $table->enum('status',['enable','disable'])->default('disable');
            $table->enum('domain', ['tattoo', 'piercing']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
