<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->constrained('categories');
            $table->string('name');
            $table->string('sku');
            $table->integer('qty');
            $table->float('discount', 3, 1);
            $table->decimal('base_price', 15, 0);
            $table->decimal('price_after_discount', 15, 0);
            $table->mediumText('short_description');
            $table->text('technical_description');
            $table->mediumText('tags');
            $table->json('properties');
            $table->enum('type', ['general','featured'])->default('general');
            $table->enum('status', ['enable', 'disable'])->default('disable');
            $table->enum('domain', ['tattoo','piercing']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
