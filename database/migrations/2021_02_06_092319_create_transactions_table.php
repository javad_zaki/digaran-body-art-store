<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained('orders');
            $table->string('res_num');
            $table->string('ref_num');
            $table->string('trace_num');
            $table->string('customer_ref_num');
            $table->string('mid');
            $table->string('card_hash_pan');
            $table->string('card_mask_pan');
            $table->string('good_ref_id');
            $table->string('merchant_data');
            $table->decimal('transaction_amount', 10, 0);
            $table->enum('state',['ok', 'canceled']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
