<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->bigInteger('user_id')->nullable();
            $table->enum('user_type', ['member', 'guest'])->nullable();
            $table->json('items');
            $table->tinyInteger('qty');
            $table->decimal('subtotal', 15, 0);
            $table->decimal('shipping', 15, 0);
            $table->decimal('discount', 15, 0);
            $table->decimal('tax', 15, 0);
            $table->decimal('total', 15, 0);
            $table->decimal('payable', 15, 0);
            $table->string('province');
            $table->string('city');
            $table->string('postal_code');
            $table->mediumText('address');
            $table->mediumText('description');
            $table->enum('status',['pending','queued','sent','returned','unpaid'])->default('pending');
            $table->enum('domain',['tattoo','piercing']);
            $table->boolean('send_paper_invoice')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
