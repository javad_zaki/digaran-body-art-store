<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            'type'                  => 'slider',
            'detail'                => json_encode(['btn_title' => 'بیشتر بدانید','btn_link' => 'https://store/product/1']),
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        DB::table('contents')->insert([
            'type'                  => 'slider',
            'detail'                => json_encode(['btn_title' => 'جزییات بیشتر','btn_link' => 'https://store/product/1']),
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        DB::table('contents')->insert([
            'type'                  => 'product_promo',
            'detail'                => json_encode(['link' => 'https://store/product/1']),
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        DB::table('contents')->insert([
            'type'                  => 'product_promo',
            'detail'                => json_encode(['link' => 'https://store/product/1']),
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    }
}
