<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artists')->insert([
            'name'                  => 'اسماعیل نصیر زاده',
            'biography'             => 'من یک تاتو آرتیست هستم.',
            'social'                => json_encode(['instagram' => 'esmaieldigaran']),
            'role'                  => 'author',
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        DB::table('artists')->insert([
            'name'                  => 'امیر حسین مولوی',
            'biography'             => 'من یک تاتو آرتیست هستم.',
            'social'                => json_encode(['instagram' => 'amirhosseinmolavi', 'facebook' => 'amirhosseinmolavi']),
            'role'                  => 'artist',
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        DB::table('artists')->insert([
            'name'                  => 'شیدا اعتمادی',
            'biography'             => 'من یک تاتو آرتیست هستم.',
            'social'                => json_encode(['instagram' => 'sheidaetemadi']),
            'role'                  => 'artist',
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        DB::table('artists')->insert([
            'name'                  => 'عرفان حق جو',
            'biography'             => 'من یک تاتو آرتیست هستم.',
            'social'                => json_encode(['instagram' => 'erfanhaghjoo']),
            'role'                  => 'artist',
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        DB::table('artists')->insert([
            'name'                  => 'ستاره سالاروند',
            'biography'             => 'من یک تاتو آرتیست هستم.',
            'social'                => json_encode(['instagram' => 'setaresalarvand']),
            'role'                  => 'artist',
            'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    }
}
